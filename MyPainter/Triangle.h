#pragma once
#include<PointVect.h>
class Triangle
{
public:
	Triangle();
	~Triangle();
	void setV0(PointVect *v0) { this->v0 = v0; };
	void setV1(PointVect *v1) { this->v1 = v1; };
	void setV2(PointVect *v2) { this->v2 = v2; };
	void computeNormal();
	void computeCenterOfMass();
	PointVect V0() const { return *v0; };
	PointVect V1() const { return *v1; };
	PointVect V2() const { return *v2; };
	PointVect Normal() const;
	PointVect CenterOfMass() const;
	bool isClockWise();
private:
	PointVect *v0;
	PointVect *v1;
	PointVect *v2;
	PointVect normal;
	PointVect centerOfMass;
};

