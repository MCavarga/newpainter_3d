#include "mypainter.h"
#include "paintwidget.h"
#include <ctime>
#include <QMessageBox>
#include <QRegExp>

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);

	ui.tableWidget_3->setRowCount(2);
	ui.tableWidget_3->setColumnCount(3);

	ui.tableWidget_3->setItem(0, 0, new QTableWidgetItem("position X"));
	ui.tableWidget_3->setItem(0, 1, new QTableWidgetItem("position Y"));
	ui.tableWidget_3->setItem(0, 2, new QTableWidgetItem("position Z"));

	for (int i = 0; i < paintWidget.Lights.size(); i++)
	{
		//light coordinates
		ui.tableWidget_3->setItem(i + 1, 0, new QTableWidgetItem(QString::number(paintWidget.Lights[i].Position().x(), 'lf', 3)));
		ui.tableWidget_3->setItem(i + 1, 1, new QTableWidgetItem(QString::number(paintWidget.Lights[i].Position().y(), 'lf', 3)));
		ui.tableWidget_3->setItem(i + 1, 2, new QTableWidgetItem(QString::number(paintWidget.Lights[i].Position().z(), 'lf', 3)));
	}

	srand(time(NULL));

	for (int i = 0; i < paintWidget.NCtrlPtsX; i++)
	{
		for (int j = 0; j < paintWidget.NCtrlPtsY; j++)
		{
			paintWidget.ControlPts.push_back(PointVect((double)i * paintWidget.defaultGap - paintWidget.NCtrlPtsX * paintWidget.defaultGap * 0.5,
													   (double)j * paintWidget.defaultGap - paintWidget.NCtrlPtsY * paintWidget.defaultGap * 0.5,
													   0.1 * (((double)(rand() % 10)) - 5.)));
		}
	}

	ui.tableWidget->setRowCount(1 + paintWidget.NCtrlPtsX * paintWidget.NCtrlPtsY);
	ui.tableWidget->setColumnCount(3);

	ui.tableWidget->setItem(0, 0, new QTableWidgetItem("X"));
	ui.tableWidget->setItem(0, 1, new QTableWidgetItem("Y"));
	ui.tableWidget->setItem(0, 2, new QTableWidgetItem("Z"));

	for (int i = 0; i < paintWidget.ControlPts.size(); i++)
	{
		//control point coordinates
		ui.tableWidget->setItem(i + 1, 0, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].x(), 'lf', 3)));
		ui.tableWidget->setItem(i + 1, 1, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].y(), 'lf', 3)));
		ui.tableWidget->setItem(i + 1, 2, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].z(), 'lf', 3)));
	}
}

void MyPainter::ActionNegative()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.fastNegative();
	QString text= "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionBlackWhite()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.blackWhite();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionSepiaTone()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.sepiaTone();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionMedian()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.medianFilter();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionSaltPepper()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.saltPepper();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionLeft()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.RotateLeft();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}

void MyPainter::ActionRight()
{
	QMessageBox mbox;
	QElapsedTimer timer;
	timer.start();
	paintWidget.RotateRight();
	QString text = "The operation took " + QString::number(timer.nsecsElapsed() / 1000000.0) + " milliseconds";
	mbox.setText(text);
	mbox.exec();
}


void MyPainter::ActionValueChanged()
{
	//evaluating rotation input
	paintWidget.sliderVal = ui.horizontalSlider->value();
	paintWidget.sliderVal2 = ui.horizontalSlider_2->value();

	//printf("rotate(phi) value: %d   ... rotate(theta) value: %d\n", paintWidget.sliderVal - paintWidget.sliderLastVal, paintWidget.sliderVal2 - paintWidget.sliderLastVal2);
	paintWidget.MeshTransformRotate(paintWidget.sliderVal - paintWidget.sliderLastVal, paintWidget.sliderVal2 - paintWidget.sliderLastVal2);
	if (paintWidget.objectBezier && !paintWidget.cameraRotation) UpdateControlPts();

	paintWidget.sliderLastVal = paintWidget.sliderVal;
	paintWidget.sliderLastVal2 = paintWidget.sliderVal2;

	//evaluating camera/planeDistance input
	paintWidget.sliderVal3 = ui.horizontalSlider_3->value();
	paintWidget.sliderVal4 = ui.horizontalSlider_4->value();
	paintWidget.CameraDistance = (ui.horizontalSlider_3->value() * 5. + 75.) / 50.; //from [1.5, 11.5]
	paintWidget.PlaneDistance = (ui.horizontalSlider_4->value() * 5. + 550.) / 500.; //from [0.5, 2.5], default: 1.5 

	paintWidget.FocalDistance = paintWidget.CameraDistance - paintWidget.PlaneDistance;

	paintWidget.ourCamera.PlaneZoom(paintWidget.CameraDistance);
	paintWidget.ourCamera.setFieldOfView(paintWidget.PlaneDistance);

	//printf("camDist = %lf \n focalDist = %lf \n", paintWidget.CameraDistance, paintWidget.FocalDistance);

	paintWidget.sliderLastVal3 = paintWidget.sliderVal3;
	paintWidget.sliderLastVal4 = paintWidget.sliderVal4;

	update();	
}


void MyPainter::ActionGenerateSphere()
{
	if (paintWidget.perspective || paintWidget.parallel)
	{
		paintWidget.objectBezier = false;
		paintWidget.objectSphere = true;
		paintWidget.divisions = ui.spinBox->value();

		ui.horizontalSlider->setValue(0);
		ui.horizontalSlider_2->setValue(0);
		ui.horizontalSlider_3->setValue(0);
		ui.horizontalSlider_4->setValue(0);

		paintWidget.surfaceBaseColor.setRed(ui.spinBox_13->value());
		paintWidget.surfaceBaseColor.setGreen(ui.spinBox_14->value());
		paintWidget.surfaceBaseColor.setBlue(ui.spinBox_15->value());

		paintWidget.CameraDistance = (ui.horizontalSlider_3->value() * 5. + 75.) / 50.; //from [1.5, 11.5]
		paintWidget.PlaneDistance = (ui.horizontalSlider_4->value() * 5. + 550.) / 500.; //from [0.5, 2.5], default: 1.5 

		PointVect cameraPosition(paintWidget.CameraDistance, 0., 0.);
		paintWidget.ourCamera = Camera(cameraPosition, paintWidget.FocalDistance);

		paintWidget.GenerateSphere(paintWidget.divisions, ui.doubleSpinBox->value(), ui.doubleSpinBox_2->value(), ui.doubleSpinBox_3->value());
	}
}

void MyPainter::ActionProjectPerspective()
{
	paintWidget.perspective = true;
	paintWidget.parallel = false;
	ui.groupBox_2->setEnabled(true);

	ui.horizontalSlider->setValue(0);
	ui.horizontalSlider_2->setValue(0);


	paintWidget.sliderVal = 0;
	paintWidget.sliderLastVal = 0;
	paintWidget.sliderVal2 = 0;
	paintWidget.sliderLastVal2 = 0;

	//evaluating camera/planeDistance input
	paintWidget.CameraDistance = (ui.horizontalSlider_3->value() * 5. + 75.) / 50.; //from [1.5, 11.5]
	paintWidget.PlaneDistance = (ui.horizontalSlider_4->value() * 5. + 550.) / 500.; //from [0.5, 2.5], default: 1.5 


	paintWidget.ourCamera.PlaneZoom(paintWidget.CameraDistance);
	paintWidget.ourCamera.setFieldOfView(paintWidget.FocalDistance);
	//printf("camDist = %lf \n focalDist = %lf \n", paintWidget.CameraDistance, paintWidget.FocalDistance);
	if (paintWidget.objectBezier && !paintWidget.cameraRotation) UpdateControlPts();
	paintWidget.MapMesh();
}

void MyPainter::ActionProjectParallel()
{
	paintWidget.perspective = false;
	paintWidget.parallel = true;
	ui.groupBox_2->setEnabled(true);

	ui.horizontalSlider->setValue(0);
	ui.horizontalSlider_2->setValue(0);

	paintWidget.sliderVal = 0;
	paintWidget.sliderLastVal = 0;
	paintWidget.sliderVal2 = 0;
	paintWidget.sliderLastVal2 = 0;
	
	paintWidget.ourCamera.PlaneZoom(paintWidget.CameraDistance);
	paintWidget.ourCamera.setFieldOfView(paintWidget.FocalDistance);
	//printf("camDist = %lf \n focalDist = %lf \n", paintWidget.CameraDistance, paintWidget.FocalDistance);
	if (paintWidget.objectBezier && !paintWidget.cameraRotation) UpdateControlPts();
	paintWidget.MapMesh();
}


void MyPainter::ActionViewMode_PointCloud()
{
	paintWidget.viewMode_Wireframe = false;
	paintWidget.viewMode_PointCloud = true;
	paintWidget.viewMode_Surfaces = false;
	paintWidget.shading = false;
	paintWidget.gouraud = false;
	printf("viewMode = PointCloud\n");
	if (paintWidget.objectBezier && paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();
	if (paintWidget.objectBezier && !paintWidget.cameraRotation) UpdateControlPts();
	paintWidget.MapMesh();
}

void MyPainter::ActionViewMode_Wireframe()
{
	paintWidget.viewMode_Wireframe = true;
	paintWidget.viewMode_PointCloud = false;
	paintWidget.viewMode_Surfaces = false;
	paintWidget.shading = false;
	paintWidget.gouraud = false;
	printf("viewMode = Wireframe\n");
	if (paintWidget.objectBezier && paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();
	if (paintWidget.objectBezier && !paintWidget.cameraRotation) UpdateControlPts();
	paintWidget.MapMesh();
}

void MyPainter::ActionViewMode_Surfaces()
{
	paintWidget.viewMode_Wireframe = false;
	paintWidget.viewMode_PointCloud = false;
	paintWidget.viewMode_Surfaces = true;
	paintWidget.shading = false;
	paintWidget.gouraud = false;
	printf("viewMode = Surfaces\n");
	if (paintWidget.objectBezier && paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();
	if (paintWidget.objectBezier && !paintWidget.cameraRotation) UpdateControlPts();
	paintWidget.MapMesh();
}

void MyPainter::ActionEnableCameraRotation()
{
	if (ui.checkBox->isChecked())
	{
		paintWidget.cameraRotation = true;
		printf("cameraRotation = true\n");
		paintWidget.MovingLights.clear();
	}
	else
	{
		paintWidget.cameraRotation = false;
		printf("cameraRotation = false\n");
	}
}

void MyPainter::ActionShadingFlat()
{
	paintWidget.viewMode_Wireframe = false;
	paintWidget.viewMode_PointCloud = false;
	paintWidget.viewMode_Surfaces = true;
	paintWidget.shading = true;
	paintWidget.gouraud = false;
	printf("viewMode = Surfaces, flat shading\n");
	if (paintWidget.objectBezier && paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();

	paintWidget.MapMesh();
}

void MyPainter::ActionShadingGouraud()
{
	paintWidget.viewMode_Wireframe = false;
	paintWidget.viewMode_PointCloud = false;
	paintWidget.viewMode_Surfaces = true;
	paintWidget.shading = true;
	paintWidget.gouraud = true;
	printf("viewMode = Surfaces, gouraud shading\n");
	if (paintWidget.objectBezier && paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();

	paintWidget.MapMesh();
}

void MyPainter::ActionAddLight()
{
	QRegExp re("[\\+\\-]*[\\d]+\\.*[\\d]*");
	if (re.exactMatch(ui.lineEdit_4->text()) && re.exactMatch(ui.lineEdit_5->text()) && re.exactMatch(ui.lineEdit_6->text()))
	{
		paintWidget.Diffusion.setRed(ui.spinBox_4->value());
		paintWidget.Diffusion.setGreen(ui.spinBox_5->value());
		paintWidget.Diffusion.setBlue(ui.spinBox_6->value());

		paintWidget.Specular.setRed(ui.spinBox_7->value());
		paintWidget.Specular.setGreen(ui.spinBox_8->value());
		paintWidget.Specular.setBlue(ui.spinBox_9->value());

		paintWidget.Ambient.setRed(ui.spinBox_10->value());
		paintWidget.Ambient.setGreen(ui.spinBox_11->value());
		paintWidget.Ambient.setBlue(ui.spinBox_12->value());

		paintWidget.shininess = ui.doubleSpinBox_4->value();

		int r = ui.tableWidget_3->rowCount();
		ui.tableWidget_3->setRowCount(r + 1);

		PointVect newLightPos(ui.lineEdit_4->text().toDouble(), ui.lineEdit_5->text().toDouble(), ui.lineEdit_6->text().toDouble());
		paintWidget.Lights.push_back(Light(newLightPos, QColor::fromRgb(255, 255, 255)));

		ui.tableWidget_3->setItem(r, 0, new QTableWidgetItem(QString::number(newLightPos.x(), 'lf', 3)));
		ui.tableWidget_3->setItem(r, 1, new QTableWidgetItem(QString::number(newLightPos.y(), 'lf', 3)));
		ui.tableWidget_3->setItem(r, 2, new QTableWidgetItem(QString::number(newLightPos.z(), 'lf', 3)));

		paintWidget.MapMesh();
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText("Error! Contents of the line edits for (x, y, z) coordinates have to be numeric!");
		msgBox.exec();
	}
}

void MyPainter::ActionRemoveLastLight()
{
	int r = ui.tableWidget_3->rowCount();
	if (r > 1)
	{
		ui.tableWidget_3->removeRow(r - 1);

		paintWidget.Lights.pop_back();

		paintWidget.MapMesh();
	}	
}

void MyPainter::ActionUpdateLightColors()
{
	paintWidget.Diffusion.setRed(ui.spinBox_4->value());
	paintWidget.Diffusion.setGreen(ui.spinBox_5->value());
	paintWidget.Diffusion.setBlue(ui.spinBox_6->value());

	paintWidget.Specular.setRed(ui.spinBox_7->value());
	paintWidget.Specular.setGreen(ui.spinBox_8->value());
	paintWidget.Specular.setBlue(ui.spinBox_9->value());

	paintWidget.Ambient.setRed(ui.spinBox_10->value());
	paintWidget.Ambient.setGreen(ui.spinBox_11->value());
	paintWidget.Ambient.setBlue(ui.spinBox_12->value());

	paintWidget.shininess = ui.doubleSpinBox_4->value();

	paintWidget.MapMesh();
}

void MyPainter::ActionClearLights()
{
	ui.tableWidget_3->clearContents();
	paintWidget.Lights.clear();
	
	ui.tableWidget_3->setRowCount(1);
	ui.tableWidget_3->setColumnCount(3);

	ui.tableWidget_3->setItem(0, 0, new QTableWidgetItem("position X"));
	ui.tableWidget_3->setItem(0, 1, new QTableWidgetItem("position Y"));
	ui.tableWidget_3->setItem(0, 2, new QTableWidgetItem("position Z"));

	paintWidget.MapMesh();
}

void MyPainter::ActionGenerateBezier()
{
	if (paintWidget.perspective || paintWidget.parallel)
	{
		paintWidget.NCtrlPtsX = ui.spinBox_2->value();
		paintWidget.NCtrlPtsY = ui.spinBox_3->value();



		if (paintWidget.defaultBezier && (ui.tableWidget->rowCount() < paintWidget.NCtrlPtsX * paintWidget.NCtrlPtsY + 1 ||
										 (paintWidget.ControlPts.size() % paintWidget.NCtrlPtsX == 0 ||
										  paintWidget.ControlPts.size() % paintWidget.NCtrlPtsY == 0)))
		{
			ActionAutoBezier();
			paintWidget.objectBezier = true;
			paintWidget.objectSphere = false;
			paintWidget.divisions = ui.spinBox->value();

			ui.horizontalSlider->setValue(0);
			ui.horizontalSlider_2->setValue(0);
			ui.horizontalSlider_3->setValue(0);
			ui.horizontalSlider_4->setValue(0);

			paintWidget.surfaceBaseColor.setRed(ui.spinBox_13->value());
			paintWidget.surfaceBaseColor.setGreen(ui.spinBox_14->value());
			paintWidget.surfaceBaseColor.setBlue(ui.spinBox_15->value());

			paintWidget.CameraDistance = (ui.horizontalSlider_3->value() * 5. + 75.) / 50.; //from [3, 13]
			paintWidget.FocalDistance = (ui.horizontalSlider_4->value() * 5. + 550.) / 500.; //from [0.5, 2.5], default: 1.5 

			PointVect cameraPosition(paintWidget.CameraDistance, 0., 0.);
			paintWidget.ourCamera = Camera(cameraPosition, paintWidget.FocalDistance);
			if (paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();
			paintWidget.GenerateBezierSurface(paintWidget.divisions);
		}
		else if (!paintWidget.defaultBezier && paintWidget.ControlPts.size() > 0 &&
				  (paintWidget.ControlPts.size() % paintWidget.NCtrlPtsX == 0 ||
				  paintWidget.ControlPts.size() % paintWidget.NCtrlPtsY == 0))
		{
			paintWidget.objectBezier = true;
			paintWidget.objectSphere = false;
			paintWidget.divisions = ui.spinBox->value();

			ui.horizontalSlider->setValue(0);
			ui.horizontalSlider_2->setValue(0);
			ui.horizontalSlider_3->setValue(0);
			ui.horizontalSlider_4->setValue(0);

			paintWidget.surfaceBaseColor.setRed(ui.spinBox_13->value());
			paintWidget.surfaceBaseColor.setGreen(ui.spinBox_14->value());
			paintWidget.surfaceBaseColor.setBlue(ui.spinBox_15->value());

			paintWidget.CameraDistance = (ui.horizontalSlider_3->value() * 5. + 75.) / 50.; //from [3, 13]
			paintWidget.FocalDistance = (ui.horizontalSlider_4->value() * 5. + 550.) / 500.; //from [0.5, 2.5], default: 1.5 

			PointVect cameraPosition(paintWidget.CameraDistance, 0., 0.);
			paintWidget.ourCamera = Camera(cameraPosition, paintWidget.FocalDistance);
			if (paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();
			paintWidget.GenerateBezierSurface(paintWidget.divisions);
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setText("Error! The control point mesh has to be an " + QString::number(paintWidget.NCtrlPtsX, 'lf', 0) +
				"x" + QString::number(paintWidget.NCtrlPtsY, 'lf', 0) + " grid, as set in the spin boxes.");
			msgBox.exec();
		}
	}

}

void MyPainter::ActionLoadBezier()
{
	QString filename = QFileDialog::getOpenFileName(
		this,
		tr("Open file with control points"),
		QDir::currentPath(),
		tr("BEZ files (*.bez)"));

	if (filename.isEmpty());
	else
	{
		QFile file(filename);
		if (!file.open(QIODevice::ReadWrite))
		{
			QMessageBox::information(this, tr("Unable to open file"), file.errorString());
		}
		QTextStream bezierfile(&file);

		ui.tableWidget->clearContents();
		paintWidget.ControlPts.clear();

		ui.tableWidget->setRowCount(1);
		ui.tableWidget->setColumnCount(3);

		ui.tableWidget->setItem(0, 0, new QTableWidgetItem("X"));
		ui.tableWidget->setItem(0, 1, new QTableWidgetItem("Y"));
		ui.tableWidget->setItem(0, 2, new QTableWidgetItem("Z"));

		paintWidget.defaultBezier = false;
		ui.checkBox_2->setChecked(false);

		

		QString line;
		line = bezierfile.readLine(100);
		line = bezierfile.readLine(100);

		paintWidget.NCtrlPtsX = line.split(" ").value(1).toInt();
		paintWidget.NCtrlPtsY = line.split(" ").value(3).toInt();

		ui.spinBox_2->setValue(paintWidget.NCtrlPtsX);
		ui.spinBox_3->setValue(paintWidget.NCtrlPtsY);
		
		ui.tableWidget->setRowCount(paintWidget.NCtrlPtsX * paintWidget.NCtrlPtsY + 1);

		double X, Y, Z;
		for (int i = 0; i < paintWidget.NCtrlPtsX * paintWidget.NCtrlPtsY; i++)
		{
			line = bezierfile.readLine(100);
			X = line.split(" ").value(0).toDouble();
			Y = line.split(" ").value(1).toDouble();
			Z = line.split(" ").value(2).toDouble();
			paintWidget.ControlPts.push_back(PointVect(X, Y, Z));

			ui.tableWidget->setItem(i + 1, 0, new QTableWidgetItem(QString::number(X, 'lf', 3)));
			ui.tableWidget->setItem(i + 1, 1, new QTableWidgetItem(QString::number(Y, 'lf', 3)));
			ui.tableWidget->setItem(i + 1, 2, new QTableWidgetItem(QString::number(Z, 'lf', 3)));
		}
		file.close();

		if (paintWidget.perspective || paintWidget.parallel)
		{
			paintWidget.objectBezier = true;
			paintWidget.objectSphere = false;
			paintWidget.divisions = ui.spinBox->value();

			ui.horizontalSlider->setValue(0);
			ui.horizontalSlider_2->setValue(0);
			ui.horizontalSlider_3->setValue(0);
			ui.horizontalSlider_4->setValue(0);

			paintWidget.surfaceBaseColor.setRed(ui.spinBox_13->value());
			paintWidget.surfaceBaseColor.setGreen(ui.spinBox_14->value());
			paintWidget.surfaceBaseColor.setBlue(ui.spinBox_15->value());

			paintWidget.CameraDistance = (ui.horizontalSlider_3->value() * 5. + 75.) / 50.; //from [3, 13]
			paintWidget.FocalDistance = (ui.horizontalSlider_4->value() * 5. + 550.) / 500.; //from [0.5, 2.5], default: 1.5 

			PointVect cameraPosition(paintWidget.CameraDistance, 0., 0.);
			paintWidget.ourCamera = Camera(cameraPosition, paintWidget.FocalDistance);
			if (paintWidget.displayBezierControlPoints) paintWidget.MapControlPoints();
			paintWidget.GenerateBezierSurface(paintWidget.divisions);
		}
	}
}

void MyPainter::ActionAutoBezier()
{
	//puts control points with default xy coords and random z-coord into the control points vector and maps Bezier
	//paintWidget.NCtrlPtsX = 4; paintWidget.NCtrlPtsY = 3;
	paintWidget.NCtrlPtsX = ui.spinBox_2->value();
	paintWidget.NCtrlPtsY = ui.spinBox_3->value();

	if (paintWidget.ControlPts.size() > 0) paintWidget.ControlPts.clear();

	srand(time(NULL));

	for (int i = 0; i < paintWidget.NCtrlPtsX; i++)
	{
		for (int j = 0; j < paintWidget.NCtrlPtsY; j++)
		{
			paintWidget.ControlPts.push_back(PointVect((double)i * paintWidget.defaultGap - paintWidget.NCtrlPtsX * paintWidget.defaultGap * 0.5,
													   (double)j * paintWidget.defaultGap - paintWidget.NCtrlPtsY * paintWidget.defaultGap * 0.5,
														0.1 * (((double)(rand() % 10)) - 5.)));
		}
	}

	ui.tableWidget->clearContents();
	ui.tableWidget->setRowCount(1 + paintWidget.NCtrlPtsX * paintWidget.NCtrlPtsY);
	ui.tableWidget->setColumnCount(3);

	ui.tableWidget->setItem(0, 0, new QTableWidgetItem("X"));
	ui.tableWidget->setItem(0, 1, new QTableWidgetItem("Y"));
	ui.tableWidget->setItem(0, 2, new QTableWidgetItem("Z"));

	for (int i = 0; i < paintWidget.ControlPts.size(); i++)
	{
		//control point coordinates
		ui.tableWidget->setItem(i + 1, 0, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].x(), 'lf', 3)));
		ui.tableWidget->setItem(i + 1, 1, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].y(), 'lf', 3)));
		ui.tableWidget->setItem(i + 1, 2, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].z(), 'lf', 3)));
	}
}

void MyPainter::ActionAddControlPoint()
{
	QRegExp re("[\\+\\-]*[\\d]+\\.*[\\d]*");
	if (re.exactMatch(ui.lineEdit->text()) && re.exactMatch(ui.lineEdit_2->text()) && re.exactMatch(ui.lineEdit_3->text()))
	{
		paintWidget.defaultBezier = false;
		ui.checkBox_2->setChecked(false);
		paintWidget.ControlPts.push_back(PointVect(ui.lineEdit->text().toDouble(), 
												   ui.lineEdit_2->text().toDouble(), 
												   ui.lineEdit_3->text().toDouble()));
		int r = ui.tableWidget->rowCount();
		ui.tableWidget->setRowCount(r + 1);

		ui.tableWidget->setItem(r, 0, new QTableWidgetItem(QString::number(ui.lineEdit->text().toDouble(), 'lf', 3)));
		ui.tableWidget->setItem(r, 1, new QTableWidgetItem(QString::number(ui.lineEdit_2->text().toDouble(), 'lf', 3)));
		ui.tableWidget->setItem(r, 2, new QTableWidgetItem(QString::number(ui.lineEdit_3->text().toDouble(), 'lf', 3)));
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText("Error! Contents of the line edits for (x, y, z) coordinates have to be numeric!");
		msgBox.exec();
	}
}

void MyPainter::ActionRemoveLastControlPoint()
{
	int r = ui.tableWidget->rowCount();
	if (r > 1)
	{
		ui.tableWidget->removeRow(r - 1);

		paintWidget.ControlPts.pop_back();
	}
}

void MyPainter::ActionSwitchDefaultBezier()
{
	if (ui.checkBox_2->isChecked())
	{
		paintWidget.defaultBezier = true;
	}
	else
	{
		paintWidget.defaultBezier = false;
	}
}

void MyPainter::UpdateControlPts()
{
	ui.tableWidget->clearContents();
	ui.tableWidget->setRowCount(1 + paintWidget.NCtrlPtsX * paintWidget.NCtrlPtsY);
	ui.tableWidget->setColumnCount(3);

	ui.tableWidget->setItem(0, 0, new QTableWidgetItem("X"));
	ui.tableWidget->setItem(0, 1, new QTableWidgetItem("Y"));
	ui.tableWidget->setItem(0, 2, new QTableWidgetItem("Z"));

	for (int i = 0; i < paintWidget.ControlPts.size(); i++)
	{
		//control point coordinates
		ui.tableWidget->setItem(i + 1, 0, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].x(), 'lf', 3)));
		ui.tableWidget->setItem(i + 1, 1, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].y(), 'lf', 3)));
		ui.tableWidget->setItem(i + 1, 2, new QTableWidgetItem(QString::number(paintWidget.ControlPts[i].z(), 'lf', 3)));
	}
}

void MyPainter::ActionDisplayBezierControlPts()
{
	if (ui.checkBox_3->isChecked())
	{
		paintWidget.displayBezierControlPoints = true;
		if (paintWidget.MeshPts.size() > 0 && paintWidget.objectBezier)
		{
			paintWidget.MapMesh();
			paintWidget.MapControlPoints();
		}
	}
	else
	{
		paintWidget.displayBezierControlPoints = false;
		if (paintWidget.MeshPts.size() > 0 && paintWidget.objectBezier)
		{
			paintWidget.MapMesh();
		}
	}
}

