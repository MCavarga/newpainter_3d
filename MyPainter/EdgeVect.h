#pragma once
#include <PointVect.h>

class EdgeVect
{
public:
	EdgeVect();
	~EdgeVect();
	void setV0(PointVect v0) { this->v0 = v0; };
	void setV1(PointVect v1) { this->v1 = v1; };
	PointVect V0() { return v0; };
	PointVect V1() { return v1; };
private:
	PointVect v0;
	PointVect v1;
};

