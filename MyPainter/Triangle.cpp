#include "Triangle.h"
#include <cmath>



Triangle::Triangle()
{
}


Triangle::~Triangle()
{
}

void Triangle::computeNormal()
{
	double nx, ny, nz, norm;

	nx = (v1->y() - v0->y()) * (v2->z() - v0->z()) - (v1->z() - v0->z()) * (v2->y() - v0->y());
	ny = (v1->z() - v0->z()) * (v2->x() - v0->x()) - (v1->x() - v0->x()) * (v2->z() - v0->z());
	nz = (v1->x() - v0->x()) * (v2->y() - v0->y()) - (v1->y() - v0->y()) * (v2->x() - v0->x());

	norm = sqrt(nx * nx + ny * ny + nz * nz);

	nx /= norm;
	ny /= norm;
	nz /= norm;

	normal.setX(nx);
	normal.setY(ny);
	normal.setZ(nz);
}

void Triangle::computeCenterOfMass()
{
	centerOfMass = PointVect(1. / 3. * (v0->x() + v1->x() + v2->x()),
							 1. / 3. * (v0->y() + v1->y() + v2->y()),
							 1. / 3. * (v0->z() + v1->z() + v2->z()));
}


PointVect Triangle::Normal() const
{
	return normal;
}

PointVect Triangle::CenterOfMass() const
{
	return centerOfMass;
}

//with respect to outward normal
bool Triangle::isClockWise()
{
	double dot = normal.x() * centerOfMass.x() + normal.y() * centerOfMass.y() + normal.z() * centerOfMass.z();

	if (dot < 0.) return true;
	else return false;
}

