#include "paintwidget.h"
#include "mypainter.h"
#include "Edgy.h"
#include "qabstractbutton.h"
#include <fstream>
#include <iostream>

//using namespace std;

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	
	sceneInit();
	
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::fastNegative()
{
	int x = image.height();
	int y = image.width();
	unsigned __int64 asd = 0x00ffffff00ffffff;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		QRgb* scanline=(QRgb*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			scanline[j] = 16777215 - scanline[j];
		}
		/*unsigned __int64* scanline=(unsigned __int64*)image.scanLine(i);
		for (int j = 0; j < y/2; j++)
		{
			scanline[j] ^= asd;
		}*/
	}
	update();
}

void PaintWidget::blackWhite()
{

	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			uchar color = (scanline[j * 4 + 0] * 0.11) + (scanline[j * 4 + 1] * 0.59) + (scanline[j * 4 + 2] * 0.3);
			scanline[j * 4 + 0] = color;
			scanline[j * 4 + 1] = color;
			scanline[j * 4 + 2] = color;
		}
	}
	/*for (int i = 0; i < image.width(); i++)
	{
		for (int j = 0; j < image.height(); j++)
		{
			QColor farba = image.pixelColor(i, j);
			farba.setRed(255 - farba.red());
			farba.setBlue(255 - farba.blue());
			farba.setGreen(255 - farba.green());
			image.setPixelColor(i, j, farba);
		}
	}*/
	update();
}

void PaintWidget::sepiaTone()
{
	int x = image.height();
	int y = image.width();
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			int colorR = scanline[j * 4 + 2] * 0.393 + scanline[j * 4 + 1] * 0.769 + scanline[j * 4 + 0] * 0.189;
			int colorG = scanline[j * 4 + 2] * 0.349 + scanline[j * 4 + 1] * 0.686 + scanline[j * 4 + 0] * 0.168;
			int colorB = scanline[j * 4 + 2] * 0.272 + scanline[j * 4 + 1] * 0.534 + scanline[j * 4 + 0] * 0.131;
			scanline[j * 4 + 2] = colorR > 255 ? 255 : colorR;
			scanline[j * 4 + 1] = colorG > 255 ? 255 : colorG;
			scanline[j * 4 + 0] = colorB > 255 ? 255 : colorB;
		}
	}
	/*
	outputRed = (inputRed * .393) + (inputGreen *.769) + (inputBlue * .189)
outputGreen = (inputRed * .349) + (inputGreen *.686) + (inputBlue * .168)
outputBlue = (inputRed * .272) + (inputGreen *.534) + (inputBlue * .131)*/
	/*for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			QRgb farba = image.pixel(j, i);
			image.setPixel(j, i, 16777215 - farba);
		}
	}*/
	update();
}

void PaintWidget::medianFilter()
{
	
	int x = image.height();
	int y = image.width();
	uchar *data = image.bits();
	int riadok = image.bytesPerLine();
#pragma omp parallel
	{
		int reds[9];
		int greens[9];
		int blues[9];
#pragma omp for
		for (int i = 1; i < x - 1; i++)
		{
			for (int j = 1; j < y - 1; j++)
			{
				int counter = 0;
				for (int k = -1; k < 2; k++)
					for (int l = -1; l < 2; l++)
					{
						blues[counter] = data[(i + k)*riadok + (j + l) * 4 + 0];
						greens[counter] = data[(i + k)*riadok + (j + l) * 4 + 1];
						reds[counter] = data[(i + k)*riadok + (j + l) * 4 + 2];
						counter++;
					}
				data[i*riadok + j * 4 + 0] = blues[selectKth(blues, 0, 9, 4)];
				data[i*riadok + j * 4 + 1] = greens[selectKth(greens, 0, 9, 4)];
				data[i*riadok + j * 4 + 2] = reds[selectKth(reds, 0, 9, 4)];
			}
		}
	}
}

void PaintWidget::saltPepper()
{
	int count = image.width()*image.height()*0.1;
	QRgb *data = (QRgb *)image.bits();
	int riadok = image.bytesPerLine()/4;
#pragma omp parallel
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> xsur(0, image.width() - 1);
		std::uniform_int_distribution<> ysur(0, image.height() - 1);
#pragma omp for
		for (int i = 0; i < count; i++)
		{
			data[ysur(gen)*riadok +xsur(gen)] = i % 2 == 0 ? 0x00000000 : 0x00ffffff;
		}
	}
}

void PaintWidget::RotateLeft()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine()/4;
	int riadok2 = druhy.bytesPerLine()/4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[i + (y-j-1)*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::RotateRight()
{
	QImage druhy(image.height(), image.width(), QImage::Format_RGB32);
	int x = image.height();
	int y = image.width();
	QRgb *data = (QRgb *)image.bits();
	QRgb *data2 = (QRgb *)druhy.bits();
	int riadok1 = image.bytesPerLine() / 4;
	int riadok2 = druhy.bytesPerLine() / 4;
#pragma omp parallel for
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			data2[x - i - 1 + j*riadok2] = data[j + i*riadok1];
		}
	}
	image = druhy;
	update();
}

void PaintWidget::Circle(int N, double r, double sx, double sy, int R, int G, int B,int width,int algorithm)
{
	int xs = image.width();
	int ys = image.height();
	double x1 , y1 , x2 , y2 ;


	for (int i = 0; i < N; i++)
	{
		x1 = (double)ys / 4. * ( sx + r * cos(i* (2 * M_PI) / N) ) + (double)xs/2. + 0.5;
		y1 = -1.*(double)ys / 4. * ( sy + r * sin(i* (2 * M_PI) / N) ) + (double)ys/2. + 0.5;

		x2 = (double)ys / 4. * ( sx + r * cos((i + 1)* (2 * M_PI) / N) ) + (double)xs/2. + 0.5;
		y2 = -1.*(double)ys / 4. * ( sy + r * sin((i + 1)* (2 * M_PI) / N) ) + (double)ys/2. + 0.5;

		if (algorithm == 0)
		{
			DDALine(QPoint((int)x1, (int)y1), QPoint((int)x2, (int)y2), R, G, B, width);
		}
		else
		{
			BresenhamLine(QPoint((int)x1, (int)y1), QPoint((int)x2, (int)y2), R, G, B, width);
		}

	}
}

void PaintWidget::DDALine(QPoint P0, QPoint P1, int R, int G, int B, int width)
{
	int x1 = std::min(P0.x(), P1.x());
	int x2 = std::max(P0.x(), P1.x());
	int y1 = (x1 == P0.x() ? P0.y() : P1.y());
	int y2 = (x2 == P1.x() ? P1.y() : P0.y());

	double distance = 0., dashing = 10.;

	QPainter painter(&image);
	int x = x1;
	int y = y1;
	double step;
	painter.setPen(QPen(QColor::fromRgb(R, G, B), width*myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor::fromRgb(R, G, B)));

	painter.drawPoint(x, y);
	if (x1 != x2)
	{
		double slope = ((double)(y1 - y2)) / ((double)(x2 - x1));

		if ((y1 - y2) <= (x2 - x1) && (y1 - y2) >= 0)  // 0 <= slope <= 1
		{
			step = y;
			while (x < x2)
			{
				x++;
				step += -slope;
				y = ((int)step + 0.5);

				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}				
			}
		}
		else if ((y1 - y2) >(x2 - x1)) // 1 < slope < infinity
		{
			step = x;
			while (y > y2)
			{
				y--;
				step += 1. / slope;
				x = ((int)step + 0.5);
				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}
			}
		}
		else if ((y2 - y1) <= (x2 - x1) && (y1 - y2) < 0) // 0 > slope >= -1
		{
			step = y;
			while (x < x2)
			{
				x++;
				step += -slope;
				y = ((int)step + 0.5);
				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}
			}
		}
		else if ((y2 - y1) >(x2 - x1) && (y2 - y1) > 0) // -1 > slope > -infinity
		{
			step = x;
			while (y < y2)
			{
				y++;
				step += -1. / slope;
				x = ((int)step + 0.5);
				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}
			}
		}
	}
	else
	{
		double dy = 1.*(y2 - y1);
		dy /= fabs(dy);
		while (y != y2)
		{
			y += (int)dy;
			distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

			if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
			{
				painter.drawPoint(x, y);
			}
			else if (!dashed)
			{
				painter.drawPoint(x, y);
			}
		}
	}
	painter.drawPoint(x2, y2);
	update();
}

void PaintWidget::BresenhamLine(QPoint P0, QPoint P1, int R, int G, int B, int width)
{
	int x1 = std::min(P0.x(), P1.x());
	int x2 = std::max(P0.x(), P1.x());
	int y1 = (x1 == P0.x() ? P0.y() : P1.y());
	int y2 = (x2 == P1.x() ? P1.y() : P0.y());

	QPainter painter(&image);

	double step;
	double distance = 0., dashing = 10;

	int x = x1;
	int y = y1;

	painter.setPen(QPen(QColor::fromRgb(R, G, B), width * myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor::fromRgb(R, G, B)));

	painter.drawPoint(x, y);

	if (x1 != x2)
	{
		if ((y1 - y2) <= (x2 - x1) && (y1 - y2) >= 0)  // 0 <= slope <= 1
		{
			int k1 = 2 * (y1 - y2);
			int k2 = 2 * (y1 - y2) - 2 * (x2 - x1);

			int p = 2 * (y1 - y2) - (x2 - x1);

			while (x < x2)
			{
				x++;

				if (p > 0)
				{
					y--;
					p += k2;
				}
				else
				{
					p += k1;
				}
				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}
			}
		}
		else if ((y1 - y2) > (x2 - x1)) // 1 < slope < infinity
		{
			int k1 = 2 * (x2 - x1);
			int k2 = 2 * (x2 - x1) - 2 * (y1 - y2);

			int p = 2 * (x2 - x1) - (y1 - y2);

			while (y > y2)
			{
				y--;

				if (p > 0)
				{
					x++;
					p += k2;
				}
				else
				{
					p += k1;
				}

				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}
			}
		}
		else if ((y2 - y1) <= (x2 - x1) && (y1 - y2) < 0) // 0 > slope >= -1
		{
			int k1 = 2 * (y2 - y1);
			int k2 = 2 * (y2 - y1) - 2 * (x2 - x1);

			int p = 2 * (y2 - y1) - (x2 - x1);

			while (x < x2)
			{
				x++;
				if (p > 0)
				{
					y++;
					p += k2;
				}
				else
				{
					p += k1;
				}
				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}
			}
		}
		else if ((y2 - y1) > (x2 - x1) && (y2 - y1) > 0) // -1 > slope > -infinity
		{
			int k1 = 2 * (x2 - x1);
			int k2 = 2 * (x2 - x1) + 2 * (y1 - y2);

			int p = 2 * (x2 - x1) + (y1 - y2);
			while (y < y2)
			{
				y++;

				if (p > 0)
				{
					x++;
					p += k2;
				}
				else
				{
					p += k1;
				}

				distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

				if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
				{
					painter.drawPoint(x, y);
				}
				else if (!dashed)
				{
					painter.drawPoint(x, y);
				}
			}
		}
	}
	else
	{
		double dy = 1.*(y2 - y1);
		dy /= fabs(dy);
		while (y != y2)
		{
			y += (int)dy;
			distance = sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1));

			if (dashed && ((int)(distance / dashing + 0.5)) % 2 == 0)
			{
				painter.drawPoint(x, y);
			}
			else if (!dashed)
			{
				painter.drawPoint(x, y);
			}
		}
	}
	painter.drawPoint(x2, y2);
	update();
}

bool cmp_YL(const Edgy &e1 , const Edgy &e2 )
{
	return (e1.Y1() > e2.Y1()) ||
		((e1.Y1() == e2.Y1()) && (e1.Y1() < e2.Y1())) ||
		((e1.Y1() == e2.Y1()) && (e1.Y1() == e2.Y1())  && (e1.W() > e2.W()) );
}

bool cmp_XL(const Edgy &e2, const Edgy &e1)
{
	return (e1.XL() > e2.XL()) ||
		((e1.XL() == e2.XL()) && (e1.XL() < e2.XL())) ||
		((e1.XL() == e2.XL()) && (e1.XL() == e2.XL()) && (e1.W() > e2.W()) );
}

bool cmp_intersectionx(const QPoint &i2, const QPoint &i1)
{
	return (i1.x() > i2.x()) ||
		((i1.x() == i2.x()) && (i1.x() < i2.x())) ||
		((i1.x() == i2.x()) && (i1.x() == i2.x()) );
}


bool cmp_PointVect_Zheight(const PointVect &V2, const PointVect &V1)
{
	return (V1.z() < V2.z()) ||
		((V1.z() == V2.z()) && (V1.z() > V2.z())) ||
		((V1.z() == V2.z()) && (V1.z() == V2.z()));
}

bool cmp_QPoint_Y(const QPoint &P0, const QPoint &P1)
{
	return (P0.y() < P1.y());
}


void PaintWidget::PolygonScanline(QPoint *coord,int N,QColor fill)
{
	QList<Edgy> edges;

	for (int i = 0; i < N - 1; i++)
	{
		if ((coord + i)->x() != (coord + i + 1)->x())
		{
			Edgy e((coord + i)->x(), (coord + i)->y(), (coord + i + 1)->x(), (coord + i + 1)->y());
			e.swap_pts();
			e.SetW(((double)(e.X1() - e.X0())) / ((double)((e.Y0() - e.Y1()))));
			edges.append(e);
			//printf("x0 = %d , y0 = %d , x1 = %d , y1 = %d \n w = %.4lf\n", e.X0(), e.Y0(), e.X1(), e.Y1(), e.W());
		}
		else
		{
			continue;
		}
	}

	if ((coord + N - 1)->x() != coord->x())
	{
		Edgy e((coord + N - 1)->x(), (coord + N - 1)->y(), coord->x(), coord->y());
		e.swap_pts();
		e.SetW(((double)(e.X1() - e.X0())) / ((double)((e.Y0() - e.Y1()))));
		edges.append(e);
		//printf("x0 = %d , y0 = %d , x1 = %d , y1 = %d \n w = %.4lf\n", e.X0(), e.Y0(), e.X1(), e.Y1(), e.W());
	}

	//printf("\n ========== Unsorted ============\n");

	for (int i = 0; i < N; i++)
	{
	//	printf("y0(%d) = %d  ....\n", edges.size() - i - 1, (int)edges.at(edges.size() - i - 1).Y1());
	}

	//printf("\n ========== Sorted ============\n");

	qSort(edges.begin(), edges.end(), cmp_YL);
	
	for (int i = 0; i < N; i++)
	{
	//	printf("y0(%d) = %d ....\n", edges.size()-i-1, (int) edges.at(edges.size()-i-1).Y1());
	}
	
	int Ymin = image.height();
	int Ymax = 0;

	for (int i = 0; i < N; i++)
	{
		if ( edges.at(i).Y1() < Ymin )
		{
			Ymin = edges.at(i).Y1();
		}
		if ( edges.at(i).Y0() > Ymax )
		{
			Ymax = edges.at(i).Y0();
		}
	}
	int Ydiff = Ymax - Ymin;
	
	QList <Edgy> active_edges;
	QList <QPoint> intersections;
	//QList <int> indices;
	int curY = Ymin;

	//printf("\n");

	for (int j = 0; j < Ydiff; j++) 
	{
		for (int i = 0; i < active_edges.size(); i++)
		{
			if (active_edges.at(i).Y0() == curY)
			{
				active_edges.removeAt(i);
				//printf("removing edge %d \n", i);
				i--;
			}
		}

		for (int i = 0; i < N; i++) 
		{
			if ( edges.at(i).Y1() == curY )
			{
				active_edges.append(edges.at(i));
				//printf("appending edge %d \n", i);
			}
		}
		qSort(active_edges.begin(), active_edges.end(), cmp_XL);

		for (int i = 0; i < active_edges.size(); i++)
		{
			QPoint inters(((int)(-active_edges.at(i).W()*curY + active_edges.at(i).W()*(active_edges.at(i).Y1()) + active_edges.at(i).X1() + 0.5)), curY);
			intersections.append(inters);
		}
		qSort(intersections.begin(), intersections.end(), cmp_intersectionx);
		//printf("intersection count = %d  , curY = %d, active edge count = %d\n", intersections.size(),curY,active_edges.size());

		for (int i = 0; i < intersections.size(); i++)
		{
		//	printf("ix(%d) = %d ", i, intersections.at(i).x());
		}
		//printf("\n");

		for (int i = 0; i < intersections.size()-1; i++)
		{
		//	printf("drawing line: (%d) ---- (%d)\n", intersections.at(i).x(), intersections.at(i+1).x());
			DDALine(QPoint(intersections.at(i).x(), curY) , QPoint(intersections.at(i + 1).x(), curY), fill.red(), fill.green(), fill.blue(),1);
			i++;
		}

		intersections.clear();
		curY++;
	}

	for (int i = 0; i < edges.size(); i++)
	{
		DDALine(QPoint(edges.at(i).X0(), edges.at(i).Y0()), QPoint(edges.at(i).X1(), edges.at(i).Y1()), 0, 0, 0, 5);
	}
}

void PaintWidget::PolygonTransformMove(int N)
{
	clearImage();
	mvX = EndingPt.x() - StartingPt.x();
	mvY = EndingPt.y() - StartingPt.y();

	for (int i = 0; i < N; i++)
	{
		vertices.at(i).setX(vertices.at(i).x() + mvX);
		vertices.at(i).setY(vertices.at(i).y() + mvY);
	}
	PolygonScanline(vertices.data(), N, myPenColor);
	//printf("moving by: (%d,%d)\n", mvX, mvY);
}

void PaintWidget::GenerateSphere(int N, double A, double B, double C)
{
	if (objectSphere)
	{
		if (Surfaces.size() > 0 || MeshPts.size() > 0)
		{
			Surfaces.clear();
			MeshPts.clear();
		}

		PointVect new_point;
		double dphi = M_PI / (2 * N);
		double dtheta = M_PI / (4 * N);
		double nx, ny, nz, norm; //normals coordinates

		std::fstream sphere("sphere.vtk", std::fstream::out);
		//printf("opening file...\n");

		sphere << "# vtk DataFile Version 3.0" << std::endl;
		sphere << "vtk output" << std::endl;
		sphere << "ASCII" << std::endl;
		sphere << "DATASET POLYDATA" << std::endl;
		sphere << "POINTS " << ((4 * N - 1) * 4 * N + 2) << " float" << std::endl;

		//printf("writing pts...\n");
		new_point.setX(0.);
		new_point.setY(0.);
		new_point.setZ(C);
		new_point.setNx(0.); new_point.setNy(0.); new_point.setNz(1.);
		new_point.setIndex(0);
		sphere << new_point.x() << " " << new_point.y() << " " << new_point.z() << std::endl;

		MeshPts.push_back(new_point);
		int k = 1;

		for (int j = 1; j < 4 * N; j++)
		{
			for (int i = 0; i < 4 * N; i++)
			{
				new_point.setX(A * cos(i * dphi) * sin(j * dtheta));
				new_point.setY(B * sin(i * dphi) * sin(j * dtheta));
				new_point.setZ(C * cos(j * dtheta));

				norm = sqrt(new_point.x() * new_point.x() + new_point.y() * new_point.y() + new_point.z() * new_point.z());

				nx = new_point.x() / norm;
				ny = new_point.y() / norm;
				nz = new_point.z() / norm;

				new_point.setNx(nx); new_point.setNy(ny); new_point.setNz(nz);
				new_point.setIndex(k);
				sphere << new_point.x() << " " << new_point.y() << " " << new_point.z() << endl;

				MeshPts.push_back(new_point);
				k++;
			}
			//rovnobezka end
		}
		new_point.setX(0.);
		new_point.setY(0.);
		new_point.setZ(-C);
		new_point.setNx(0.); new_point.setNy(0.); new_point.setNz(-1.);
		new_point.setIndex(k);
		sphere << new_point.x() << " " << new_point.y() << " " << new_point.z() << endl;

		MeshPts.push_back(new_point);

		//printf("MeshPts.size() = %zd\n", MeshPts.size());
		//printf("preparing triangles...\n");


		Triangle new_triangle;

		new_triangle.setV0(&MeshPts.at(0));
		new_triangle.setV1(&MeshPts.at(4 * N));
		new_triangle.setV2(&MeshPts.at(1));
		new_triangle.computeNormal();
		new_triangle.computeCenterOfMass();

		//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));

		Surfaces.push_back(new_triangle);

		//printf("(%d)...(%d)...(%d)\n", 0, 4 * N, 1);

		for (int i = 1; i < 4 * N; i++)
		{
			new_triangle.setV1(&MeshPts.at(i));
			new_triangle.setV2(&MeshPts.at(i + 1));
			new_triangle.computeNormal();
			new_triangle.computeCenterOfMass();
			//printf("(%d)...(%d)...(%d)\n", 0, i, i + 1);

			//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));

			Surfaces.push_back(new_triangle);
		}

		for (int j = 0; j < 4 * N - 2; j++)
		{
			for (int i = 0; i < 4 * N - 1; i++)
			{
				new_triangle.setV0(&MeshPts.at((4 * N) * j + i + 1));
				new_triangle.setV1(&MeshPts.at((4 * N) * (j + 1) + i + 1));
				new_triangle.setV2(&MeshPts.at((4 * N) * j + i + 2));
				new_triangle.computeNormal();
				new_triangle.computeCenterOfMass();
				//printf("(%d)...(%d)...(%d)\n", (4 * N) * j + i + 1, (4 * N) * (j + 1) + i + 1, (4 * N) * j + i + 2);

				//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));

				Surfaces.push_back(new_triangle);

				new_triangle.setV0(&MeshPts.at((4 * N) * j + i + 2));
				new_triangle.setV1(&MeshPts.at((4 * N) * (j + 1) + i + 1));
				new_triangle.setV2(&MeshPts.at((4 * N) * (j + 1) + i + 2));
				new_triangle.computeNormal();
				new_triangle.computeCenterOfMass();
				//printf("(%d)...(%d)...(%d)\n", (4 * N) * j + i + 2, (4 * N)*(j + 1) + i + 1, (4 * N) * (j + 1) + i + 2);

				//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));

				Surfaces.push_back(new_triangle);
			}

			new_triangle.setV0(&MeshPts.at((4 * N) * j + 4 * N));
			new_triangle.setV1(&MeshPts.at((4 * N) * (j + 1) + 4 * N));
			new_triangle.setV2(&MeshPts.at((4 * N) * j + 1));
			new_triangle.computeNormal();
			new_triangle.computeCenterOfMass();
			//printf("(%d)...(%d)...(%d)\n", (4 * N) * j + 4 * N, (4 * N) * (j + 1) + 4 * N, (4 * N) * j + 1);
			//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));
			Surfaces.push_back(new_triangle);

			new_triangle.setV0(&MeshPts.at((4 * N) * j + 1));
			new_triangle.setV1(&MeshPts.at((4 * N) * (j + 1) + 4 * N));
			new_triangle.setV2(&MeshPts.at((4 * N) * (j + 1) + 1));
			new_triangle.computeNormal();
			new_triangle.computeCenterOfMass();
			//printf("(%d)...(%d)...(%d)\n", (4 * N) * j + 1, (4 * N) * (j + 1) + 4 * N, (4 * N) * (j + 1) + 1);
			//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));

			Surfaces.push_back(new_triangle);
		}

		//printf("MeshPts.size() = %zd\n",MeshPts.size());

		new_triangle.setV0(&MeshPts.at(MeshPts.size() - 1));
		new_triangle.setV1(&MeshPts.at(MeshPts.size() - 4 * N - 1));
		new_triangle.setV2(&MeshPts.at(MeshPts.size() - 2));
		new_triangle.computeNormal();
		new_triangle.computeCenterOfMass();
		//printf("(%zd)...(%zd)...(%zd)\n", MeshPts.size() - 1, MeshPts.size() - 4 * N - 1, MeshPts.size() - 2);
		//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));

		Surfaces.push_back(new_triangle);

		for (int i = 0; i < 4 * N - 1; i++)
		{
			new_triangle.setV1(&MeshPts.at(MeshPts.size() - 4 * N + i));
			new_triangle.setV2(&MeshPts.at(MeshPts.size() - 4 * N + i - 1));
			new_triangle.computeNormal();
			new_triangle.computeCenterOfMass();
			//printf("(%zd)...(%zd)...(%zd)\n", MeshPts.size() - 1, MeshPts.size() - 4 * N + i, MeshPts.size() - 4 * N + i - 1);
			//printf("triangle : %s\n", (new_triangle.isClockWise() ? "clockwise" : "anti-clockwise"));
			Surfaces.push_back(new_triangle);
		}

		//printf("writing triangles...\n");
		sphere << "POLYGONS " << Surfaces.size() << " " << 4 * Surfaces.size() << std::endl;


		for (int i = 0; i < Surfaces.size(); i++)
		{
			sphere << "3 " << Surfaces.at(i).V0().idx() << " " << Surfaces.at(i).V1().idx() << " " << Surfaces.at(i).V2().idx() << std::endl;
		}
		//printf("closing file...\n");
		sphere.close();

		MapMesh();
	}
	
}

void PaintWidget::GenerateBezierSurface(int N)
{
	if (objectBezier)
	{
		//------------------------------------
		std::fstream bezierctrlfile("bezier.bez", std::fstream::out); //writing down control point array

		bezierctrlfile << "# Bezier control point coordinates" << std::endl;
		bezierctrlfile << "rows " << NCtrlPtsX << " columns " << NCtrlPtsY << std::endl;

		for (int i = 0; i < ControlPts.size(); i++)
		{
			bezierctrlfile << ControlPts[i].x() << " " << ControlPts[i].y() << " " << ControlPts[i].z() << std::endl;
		}

		bezierctrlfile.close();

		//------------------------------------

		if (MeshPts.size() > 0 || Surfaces.size() > 0)
		{
			MeshPts.clear(); Surfaces.clear();
		}

		std::vector<PointVect> ControlPointRow;
		std::vector<PointVect> ControlPointColumn;
		std::vector<PointVect> IntermediateControlPts; //control points generated by de-Casteljau Bezier curves from ControlPointRow points
		double t;

		for (int y = 0; y < NCtrlPtsX; y++)
		{
			for (int x = 0; x < NCtrlPtsY; x++)
			{
				ControlPointRow.push_back(ControlPts[y * NCtrlPtsY + x]);
			}
			//from these control points a set of BezierCurve points will be computed using de Casteljau
			std::vector<PointVect> P1 (NCtrlPtsY);
			std::vector<PointVect> P2 (NCtrlPtsY);
			PointVect Prev;

			P1 = ControlPointRow;
			Prev = ControlPointRow[0];

			for (int k = 0; k <= N + 1; k++)
			{
				t = k * (1. / N); //parametrizing from [0, 1]
				for (int j = 1; j < NCtrlPtsY; j++)
				{
					for (int i = 0; i < NCtrlPtsY - j; i++)
					{
						P2[i] = PointVect((1 - t) * P1[i].x() + t * P1[i + 1].x(),
										  (1 - t) * P1[i].y() + t * P1[i + 1].y(),
										  (1 - t) * P1[i].z() + t * P1[i + 1].z());
					}
					P1 = P2;
				}
				if (k > 0) IntermediateControlPts.push_back(Prev);
				//if (k > 0) MeshPts.push_back(Prev);
				Prev = P1[0];
				P1 = ControlPointRow;
			}
			P1.clear(); P2.clear();
			ControlPointRow.clear();
		}
		//-----------------------------------------------------------

		std::fstream beziervtk("bezier.vtk", std::fstream::out);

		beziervtk << "# vtk DataFile Version 3.0" << std::endl;
		beziervtk << "vtk output" << std::endl;
		beziervtk << "ASCII" << std::endl;
		beziervtk << "DATASET POLYDATA" << std::endl;
		beziervtk << "POINTS " << (N + 1) * (N + 1) << " float" << std::endl;

		//essentially here I have an N by NCtrlPtsX array of control points, which need to be 'de-Casteljau'-ed with respect to the x-axis
		//so the process is repeated, but with columns

		for (int y = 0; y < N + 1; y++)
		{
			for (int x = 0; x < NCtrlPtsX; x++)
			{
				ControlPointColumn.push_back(IntermediateControlPts[x * (N + 1) + y]);
			}

			std::vector<PointVect> P1(NCtrlPtsX);
			std::vector<PointVect> P2(NCtrlPtsX);
			std::vector<PointVect> Ptangents(NCtrlPtsX);
			PointVect Prev;

			P1 = ControlPointColumn;
			Prev = ControlPointColumn[0];

			for (int k = 0; k <= N + 1; k++)
			{
				t = k * (1. / N); //parametrizing from [0, 1]
				for (int j = 1; j < NCtrlPtsX; j++)
				{
					for (int i = 0; i < NCtrlPtsX - j; i++)
					{
						P2[i] = PointVect((1 - t) * P1[i].x() + t * P1[i + 1].x(),
										  (1 - t) * P1[i].y() + t * P1[i + 1].y(),
										  (1 - t) * P1[i].z() + t * P1[i + 1].z());
						Ptangents[i] = PointVect(P1[i + 1].x() - P1[i].x(),
												 P1[i + 1].y() - P1[i].y(),
												 P1[i + 1].z() - P1[i].z());
					}
					P1 = P2;
				}
				if (k > 0)
				{
					normalize(Ptangents[0]);
					Prev.setNx(-Ptangents[0].z()); 
					Prev.setNy(Ptangents[0].y());
					Prev.setNz(Ptangents[0].x());
					MeshPts.push_back(Prev);
					beziervtk << Prev.x() << " " << Prev.y() << " " << Prev.z() << std::endl;
				}
				Prev = P1[0];
				P1 = ControlPointColumn;
			}
			P1.clear(); P2.clear();
			ControlPointColumn.clear();
		}

		//===== Vertex Normals ==========================
		//an approximate way to do it

		/*
		PointVect Tangent1, Tangent2, Normal;

		for (int i = 0; i < MeshPts.size() - N - 1; i++)
		{
			if ((i + 1) % (N + 1) == 0 && i > 0) //last in the row
			{
				Tangent1 = PointVect(MeshPts[i].x() - MeshPts[i + 1].x(),
									 MeshPts[i].y() - MeshPts[i + 1].y(),
									 MeshPts[i].z() - MeshPts[i + 1].z());
				Tangent2 = PointVect(MeshPts[i].x() - MeshPts[i + N + 1].x(),
									 MeshPts[i].y() - MeshPts[i + N + 1].y(),
									 MeshPts[i].z() - MeshPts[i + N + 1].z());
				normalize(Tangent1); normalize(Tangent2);
				Normal = cross(Tangent1, Tangent2);
				MeshPts[i].setNx(-Normal.x());
				MeshPts[i].setNy(-Normal.y());
				MeshPts[i].setNz(-Normal.z());
			}
			else //other
			{
				Tangent1 = PointVect(MeshPts[i + 1].x() - MeshPts[i].x(),
									 MeshPts[i + 1].y() - MeshPts[i].y(), 
									 MeshPts[i + 1].z() - MeshPts[i].z());
				Tangent2 = PointVect(MeshPts[i + N + 1].x() - MeshPts[i].x(),
									 MeshPts[i + N + 1].y() - MeshPts[i].y(), 
									 MeshPts[i + N + 1].z() - MeshPts[i].z());
				normalize(Tangent1); normalize(Tangent2);
				Normal = cross(Tangent1, Tangent2);
				MeshPts[i].setNx(Normal.x());
				MeshPts[i].setNy(Normal.y());
				MeshPts[i].setNz(Normal.z());
			}
		}*/

		//===== Triangulation ============================		

		Triangle new_triangle;
		int NSurf = 2 * N * N;

		beziervtk << "POLYGONS " << NSurf << " " << 4 * NSurf << std::endl;

		for (int i = 0; i < MeshPts.size() - N - 1; i++)
		{
			if ((i + 1) % (N + 1) == 0 && i > 0)
			{
				continue;
			}
			else
			{
				new_triangle.setV0(&MeshPts.at(i));
				new_triangle.setV1(&MeshPts.at(i + N + 2));
				new_triangle.setV2(&MeshPts.at(i + N + 1));
				new_triangle.computeNormal();
				new_triangle.computeCenterOfMass();

				beziervtk << "3 " << i << " " << i + N + 2 << " " << i + N + 1 << std::endl;

				Surfaces.push_back(new_triangle);

				new_triangle.setV0(&MeshPts.at(i));
				new_triangle.setV1(&MeshPts.at(i + 1));
				new_triangle.setV2(&MeshPts.at(i + N + 2));
				new_triangle.computeNormal();
				new_triangle.computeCenterOfMass();

				beziervtk << "3 " << i << " " << i + 1 << " " << i + N + 2 << std::endl;

				Surfaces.push_back(new_triangle);
			}
		}
		beziervtk.close();

		MapMesh();
		if (displayBezierControlPoints) MapControlPoints();
	}
}


void PaintWidget::MapMesh()
{
	if (MeshPts.size() > 0)
	{
		clearImage();

		int xs = image.width();
		int ys = image.height();

		PointVect P0, P1, P2; //camera points
		QPoint V0, V1, V2; //canvas points

		/*
		printf("\n some point:\n");
		double tryX = 1., tryY = 0., tryZ = 0.;
		printf("in global coords: (%lf, %lf, %lf)\n", tryX, tryY, tryZ);
		P0 = ourCamera.GlobalToCameraCoord(PointVect(tryX, tryY, tryZ));
		printf("in camera coords: (%lf, %lf, %lf)\n", P0.x(), P0.y(), P0.z());
		V0 = ourCamera.CameraToCanvasCoord(P0, xs, ys, (perspective && !parallel));
		printf("in canvas coords: (%d, %d)\n", V0.x(), V0.y());*/

		// Wireframe Mode
		if (viewMode_Wireframe == true && viewMode_PointCloud == false && viewMode_Surfaces == false)
		{
			for (int i = 0; i < Surfaces.size(); i++)
			{
				P0 = ourCamera.GlobalToCameraCoord(Surfaces[i].V0());
				P1 = ourCamera.GlobalToCameraCoord(Surfaces[i].V1());
				P2 = ourCamera.GlobalToCameraCoord(Surfaces[i].V2());

				V0 = ourCamera.CameraToCanvasCoord(P0, xs, ys, (perspective && !parallel));
				V1 = ourCamera.CameraToCanvasCoord(P1, xs, ys, (perspective && !parallel));
				V2 = ourCamera.CameraToCanvasCoord(P2, xs, ys, (perspective && !parallel));

				DDALine(V0, V1, 0, 0, 0, 1);
				DDALine(V1, V2, 0, 0, 0, 1);
				DDALine(V2, V0, 0, 0, 0, 1);
			}
		}
		// Point Cloud Mode
		else if (viewMode_Wireframe == false && viewMode_PointCloud == true && viewMode_Surfaces == false) 
		{
			for (int i = 0; i < MeshPts.size(); i++)
			{
				P0 = ourCamera.GlobalToCameraCoord(MeshPts[i]);
				V0 = ourCamera.CameraToCanvasCoord(P0, xs, ys, (perspective && !parallel));

				//normals
				P1 = PointVect(MeshPts[i].x() + MeshPts[i].Nx(), MeshPts[i].y() + MeshPts[i].Ny(), MeshPts[i].z() + MeshPts[i].Nz());
				P2 = ourCamera.GlobalToCameraCoord(P1);
				V1 = ourCamera.CameraToCanvasCoord(P2, xs, ys, (perspective && !parallel));

				DDALine(QPoint(V0.x() - 2, V0.y()), QPoint(V0.x() + 2, V0.y()), 0, 0, 0, 4);
				DDALine(QPoint(V0.x(), V0.y() - 2), QPoint(V0.x(), V0.y() + 2), 0, 0, 0, 4);

				writeIndex(i, QPoint(V0.x() + 5, V0.y() + 5));

				DDALine(V0, V1, 0, 150, 100, 1); //draw normals
			}
		}
		// Surface Mesh Mode
		else if (viewMode_Wireframe == false && viewMode_PointCloud == false && viewMode_Surfaces == true) 
		{
			double d0, d1, d2; //distances for z-buffer
			PointVect normal0, normal1, normal2;
			PointVect surfaceNormal, surfaceCenter;

			//fill the buffer array xs times ys
			DepthBuffer = new double *[xs];

			for (int i = 0; i < xs; i++)
			{
				DepthBuffer[i] = new double[ys];
			}

			for (int i = 0; i < xs; i++)
			{
				for (int j = 0; j < ys; j++)
				{
					DepthBuffer[i][j] = HUGE_VALF;
				}
			}

			for (int i = 0; i < Surfaces.size(); i++)
			{
				P0 = ourCamera.GlobalToCameraCoord(Surfaces[i].V0());
				P1 = ourCamera.GlobalToCameraCoord(Surfaces[i].V1());
				P2 = ourCamera.GlobalToCameraCoord(Surfaces[i].V2());

				V0 = ourCamera.CameraToCanvasCoord(P0, xs, ys, (perspective && !parallel));
				V1 = ourCamera.CameraToCanvasCoord(P1, xs, ys, (perspective && !parallel));
				V2 = ourCamera.CameraToCanvasCoord(P2, xs, ys, (perspective && !parallel));

				if (perspective)
				{
					//just take norms of the points in camera coordinates
					d0 = norm(P0);
					d1 = norm(P1);
					d2 = norm(P2);
				}
				else if (parallel)
				{
					d0 = ourCamera.DistanceToPlaneCamera(P0);
					d1 = ourCamera.DistanceToPlaneCamera(P1);
					d2 = ourCamera.DistanceToPlaneCamera(P2);
				}

				if (shading && gouraud) //Gouraud shading
				{
					normal0 = PointVect(Surfaces[i].V0().Nx(), Surfaces[i].V0().Ny(), Surfaces[i].V0().Nz());
					normal1 = PointVect(Surfaces[i].V1().Nx(), Surfaces[i].V1().Ny(), Surfaces[i].V1().Nz());
					normal2 = PointVect(Surfaces[i].V2().Nx(), Surfaces[i].V2().Ny(), Surfaces[i].V2().Nz());

					//we need to provide vertices and normals in GLOBAL coordinates as arguments in for the shading algorithm to work
					PixelTriangleScanline(&Surfaces[i].V0(), &Surfaces[i].V1(), &Surfaces[i].V2(), &normal0, &normal1, &normal2, V0, V1, V2, d0, d1, d2);
				}
				else if (shading && !gouraud) //flat shading
				{
					surfaceNormal = Surfaces[i].Normal();
					surfaceCenter = Surfaces[i].CenterOfMass();

					//we need to provide vertices and normals in GLOBAL coordinates as arguments in for the shading algorithm to work
					PixelTriangleScanline(&Surfaces[i].Normal(), &Surfaces[i].CenterOfMass(), V0, V1, V2, d0, d1, d2);
				}
				else //no shading, just surfaces
				{
					PixelTriangleScanline(V0, V1, V2, d0, d1, d2);
				}
			}

			//dealloc the buffer array
			for (int i = 0; i < ys; i++)
			{
				delete[] DepthBuffer[i];
			}

			delete[] DepthBuffer;
		}		
	}
}

void PaintWidget::MeshTransformRotate(int value1, int value2)
{
	double phi = ((double)value1) / (360.) * 2 * M_PI;
	double theta = ((double)value2) / (90.) * M_PI;

	//printf("phi = %lf , theta = %lf\n", phi, theta);

	if (cameraRotation)
	{
		ourCamera.RotateCamera(phi, theta);

		MapMesh();
		if (objectBezier && displayBezierControlPoints) MapControlPoints();
	}
	else //transforms the actual object with global coordinates
	{
		clearImage();

		for (int i = 0; i < MeshPts.size(); i++)
		{
			rotatedX = cos(theta) * (cos(phi) * MeshPts[i].x() - sin(phi) * MeshPts[i].y()) - sin(theta) * MeshPts[i].z();
			rotatedY = sin(phi) * MeshPts[i].x() + cos(phi) * MeshPts[i].y();
			rotatedZ = sin(theta) * (cos(phi) * MeshPts[i].x() - sin(phi) * MeshPts[i].y()) + cos(theta) * MeshPts[i].z();

			MeshPts[i].setX(rotatedX);
			MeshPts[i].setY(rotatedY);
			MeshPts[i].setZ(rotatedZ);

			rotatedX = cos(theta) * (cos(phi) * MeshPts[i].Nx() - sin(phi) * MeshPts[i].Ny()) - sin(theta) * MeshPts[i].Nz();
			rotatedY = sin(phi) * MeshPts[i].Nx() + cos(phi) * MeshPts[i].Ny();
			rotatedZ = sin(theta) * (cos(phi) * MeshPts[i].Nx() - sin(phi) * MeshPts[i].Ny()) + cos(theta) * MeshPts[i].Nz();

			MeshPts[i].setNx(rotatedX);
			MeshPts[i].setNy(rotatedY);
			MeshPts[i].setNz(rotatedZ);
		}

		//new normals and centers of mass
		for (int i = 0; i < Surfaces.size(); i++)
		{
			Surfaces[i].computeCenterOfMass();
			Surfaces[i].computeNormal();
		}

		//printf("--------------------------\n");
		//printf("cameraPos = (%lf, %lf, %lf)\n", ourCamera.FocusPosition().x(), ourCamera.FocusPosition().y(), ourCamera.FocusPosition().z());
		//printf("lightPos = (%lf, %lf, %lf)\n", Lights[0].Position().x(), Lights[0].Position().y(), Lights[0].Position().z());

		MapMesh();
		if (objectBezier)
		{
			for (int i = 0; i < ControlPts.size(); i++)
			{
				rotatedX = cos(theta) * (cos(phi) * ControlPts[i].x() - sin(phi) * ControlPts[i].y()) - sin(theta) * ControlPts[i].z();
				rotatedY = sin(phi) * ControlPts[i].x() + cos(phi) * ControlPts[i].y();
				rotatedZ = sin(theta) * (cos(phi) * ControlPts[i].x() - sin(phi) * ControlPts[i].y()) + cos(theta) * ControlPts[i].z();

				ControlPts[i].setX(rotatedX);
				ControlPts[i].setY(rotatedY);
				ControlPts[i].setZ(rotatedZ);
			}

			if (displayBezierControlPoints) MapControlPoints();
		}
	}
}

bool PaintWidget::insideCanvas(QPoint X)
{
	if (X.x() < 0 || X.x() >= image.width()) return false;
	if (X.y() < 0 || X.y() >= image.height()) return false;
	return true;
}

bool PaintWidget::insideCanvas(int x, int y)
{
	if (x < 0 || x >= image.width()) return false;
	if (y < 0 || y >= image.height()) return false;
	return true;
}

bool PaintWidget::isZeroArea(QPoint V0, QPoint V1, QPoint V2)
{
	double Area = 0.5 * fabs((double)((V1.x() - V0.x()) * (V2.y() - V0.y()) - (V1.y() - V0.y()) * (V2.x() - V0.x())));
	if (Area > -DBL_MIN && Area < DBL_MIN)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void PaintWidget::bufferBetweenPts(QPoint V0, QPoint V1, double d0, double d1)
{
	if (V0.y() != V1.y())
	{
		printf("bufferBetweenPts ERROR! Points do not form a horizontal line segment!\n");
	}
	else
	{
		QPainter painter(&image);
		int y = V0.y();
		double lambda, dd0, dd1;
		int xL = (V0.x() < V1.x() ? V0.x() : V1.x());
		dd0 = (V0.x() < V1.x() ? d0 : d1);
		int xR = (V0.x() < V1.x() ? V1.x() : V0.x());
		dd1 = (V0.x() < V1.x() ? d1 : d0);

		double currentD;

		painter.setPen(QPen(QColor::fromRgb(0, 0, 0), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));

		for (int x = xL; x <= xR; x++)
		{
			if (!insideCanvas(x, y)) continue;

			lambda = ((double)(x - xL)) / ((double)(xR - xL));
			currentD = (1 - lambda) * d0 + lambda * d1;
			
			if (currentD < DepthBuffer[x][y])
			{
				DepthBuffer[x][y] = currentD;
				painter.drawPoint(x, y);
			}
		}

		update();
	}
}

double PaintWidget::interpolateDistance(int x, int y, QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2)
{
	//Barycentric interpolation of distance defined on vertices V0, V1, and V2
	double Area, Area0, Area1, Area2;

	Area = 0.5 * fabs((double) ((V1.x() - V0.x()) * (V2.y() - V0.y()) - (V1.y() - V0.y()) * (V2.x() - V0.x())) );
	Area0 = 0.5 * fabs((double) ((x - V1.x()) * (V2.y() - V1.y()) - (y - V1.y()) * (V2.x() - V1.x())) );
	Area1 = 0.5 * fabs((double) ((x - V0.x()) * (V2.y() - V0.y()) - (y - V0.y()) * (V2.x() - V0.x())) );
	Area2 = 0.5 * fabs((double) ((V1.x() - V0.x()) * (y - V0.y()) - (V1.y() - V0.y()) * (x - V0.x())) );

	if (Area > -DBL_MIN && Area < DBL_MIN)
	{
		//printf("zero triangle area! setting interpolated distance to average.\n");
		return (d0 + d1 + d2) / 3.;
	}
	else
	{
		return (d0 * Area0 + d1 * Area1 + d2 * Area2) / Area;
	}	
}

QColor PaintWidget::interpolateColor(int x, int y, QPoint V0, QPoint V1, QPoint V2, QColor C0, QColor C1, QColor C2)
{
	//Barycentric interpolation of QColor defined on vertices V0, V1, and V2
	double Area, Area0, Area1, Area2;

	Area = 0.5 * fabs((double)((V1.x() - V0.x()) * (V2.y() - V0.y()) - (V1.y() - V0.y()) * (V2.x() - V0.x())));
	Area0 = 0.5 * fabs((double)((x - V1.x()) * (V2.y() - V1.y()) - (y - V1.y()) * (V2.x() - V1.x())));
	Area1 = 0.5 * fabs((double)((x - V0.x()) * (V2.y() - V0.y()) - (y - V0.y()) * (V2.x() - V0.x())));
	Area2 = 0.5 * fabs((double)((V1.x() - V0.x()) * (y - V0.y()) - (V1.y() - V0.y()) * (x - V0.x())));

	if (Area > -DBL_MIN && Area < DBL_MIN)
	{
		//printf("zero triangle area! setting interpolated color to average.\n");
		return QColor::fromRgb((int)(((double)C0.red() + (double)C1.red() + (double)C2.red()) / 3. + 0.5),
							   (int)(((double)C0.green() + (double)C1.green() + (double)C2.green()) / 3. + 0.5),
							   (int)(((double)C0.blue() + (double)C1.blue() + (double)C2.blue()) / 3. + 0.5));
	}
	else
	{
		return QColor::fromRgb((int)(((double)C0.red() * Area0 + (double)C1.red() * Area1 + (double)C2.red() * Area2) / Area + 0.5),
							   (int)(((double)C0.green() * Area0 + (double)C1.green() * Area1 + (double)C2.green() * Area2) / Area + 0.5),
							   (int)(((double)C0.blue() * Area0 + (double)C1.blue() * Area1 + (double)C2.blue() * Area2) / Area + 0.5));
	}
}

void PaintWidget::MapControlPoints()
{
	if (objectBezier && displayBezierControlPoints)
	{
		int xs = image.width();
		int ys = image.height();

		PointVect P0, P1, P2, P3; //camera points
		QPoint V0, V1, V2, V3; //canvas points

		for (int i = 0; i < ControlPts.size(); i++)
		{
			P0 = ourCamera.GlobalToCameraCoord(ControlPts[i]);
			V0 = ourCamera.CameraToCanvasCoord(P0, xs, ys, (perspective && !parallel));

			DDALine(QPoint(V0.x() - 2, V0.y()), QPoint(V0.x() + 2, V0.y()), 255, 0, 0, 4);
			DDALine(QPoint(V0.x(), V0.y() - 2), QPoint(V0.x(), V0.y() + 2), 255, 0, 0, 4);

			writeIndex(i, QPoint(V0.x() + 5, V0.y() + 5));

			QPainter painter(&image);

			painter.setPen(QPen(QColor::fromRgb(255, 255, 255), 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
			painter.setBrush(QBrush(QColor::fromRgb(255, 255, 255)));
			painter.drawPoint(V0.x(), V0.y());

			update();
		}

		//dashed lines
		dashed = true;
		for (int i = 0; i < ControlPts.size() - NCtrlPtsY - 1; i++)
		{
			if ((i + 1) % NCtrlPtsY == 0 && i > 0) continue;
			else
			{
				P0 = ourCamera.GlobalToCameraCoord(ControlPts[i]);
				P1 = ourCamera.GlobalToCameraCoord(ControlPts[i + 1]);
				P2 = ourCamera.GlobalToCameraCoord(ControlPts[i + NCtrlPtsY]);
				P3 = ourCamera.GlobalToCameraCoord(ControlPts[i + NCtrlPtsY + 1]);

				V0 = ourCamera.CameraToCanvasCoord(P0, xs, ys, (perspective && !parallel));
				V1 = ourCamera.CameraToCanvasCoord(P1, xs, ys, (perspective && !parallel));
				V2 = ourCamera.CameraToCanvasCoord(P2, xs, ys, (perspective && !parallel));
				V3 = ourCamera.CameraToCanvasCoord(P3, xs, ys, (perspective && !parallel));

				DDALine(V0, V1, 170, 0, 0, 1);
				DDALine(V0, V2, 170, 0, 0, 1);
				DDALine(V1, V3, 170, 0, 0, 1);
			}
		}

		for (int i = ControlPts.size() - NCtrlPtsY; i < ControlPts.size() - 1; i++)
		{
			P0 = ourCamera.GlobalToCameraCoord(ControlPts[i]); 
			P1 = ourCamera.GlobalToCameraCoord(ControlPts[i + 1]);

			V0 = ourCamera.CameraToCanvasCoord(P0, xs, ys, (perspective && !parallel));
			V1 = ourCamera.CameraToCanvasCoord(P1, xs, ys, (perspective && !parallel));

			DDALine(V0, V1, 170, 0, 0, 1);
		}
	}
	dashed = false;
}

void PaintWidget::writeIndex(int i, QPoint position)
{
	QPainter textPainter(&image);
	textPainter.setPen(QPen(QColor::fromRgb(0, 0, 0), 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	textPainter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));

	textPainter.drawText(position, QString::number(i));

	update();
}


PointVect PaintWidget::ReflectionVector(Light L, PointVect from, PointVect normal)
{
	PointVect result;
	PointVect lightVector = L.PositionFrom(&from);

	double S = dot(lightVector, normal);

	result.setX(2. * S * normal.x() - lightVector.x());
	result.setY(2. * S * normal.y() - lightVector.y());
	result.setZ(2. * S * normal.z() - lightVector.z());

	return result;
}


void PaintWidget::upperTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2)
{
	QPainter painter(&image);

	double w0, w1; //reciprocal slopes

	w0 = ((double)(V0.x() - V2.x()) / (double)(V0.y() - V2.y()));
	w1 = ((double)(V1.x() - V2.x()) / (double)(V1.y() - V2.y()));


	int Ydiff = V2.y() - V0.y(); //how many rows to fill
	//integer x-boundaries
	int xL = V0.x();
	int xR = V1.x();
	//these are the x-boundaries to be rounded off
	double xLeft = (double)xL;
	double xRight = (double)xR;

	//interpolated distance
	double d;
		
	for (int y = V0.y(); y <= V2.y(); y++)
	{
		for (int x = xL; x <= xR; x++)
		{
			if (!insideCanvas(x, y)) continue;

			d = interpolateDistance(x, y, V0, V1, V2, d0, d1, d2);

			if (d < DepthBuffer[x][y])
			{
				DepthBuffer[x][y] = d;

				if (x == xL || x == xR) //black edges
				{
					painter.setPen(QPen(QColor::fromRgb(0, 0, 0), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));
				}
				else //surface colored interior
				{
					painter.setPen(QPen(surfaceBaseColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(surfaceBaseColor));
				}

				painter.drawPoint(x, y);
			}
		}
		xLeft += w0;
		xRight += w1;
		xL = ((int)(xLeft + 0.5));
		xR = ((int)(xRight + 0.5));
	}

	update();
}

void PaintWidget::lowerTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2)
{
	QPainter painter(&image);

	double w0, w1; //reciprocal slopes

	w0 = ((double)(V0.x() - V2.x()) / (double)(V2.y() - V0.y()));
	w1 = ((double)(V1.x() - V0.x()) / (double)(V0.y() - V1.y()));

	int Ydiff = V2.y() - V0.y(); //how many rows to fill
	//integer x-boundaries
	int xL = V2.x();
	int xR = V1.x();
	//these are the x-boundaries to be rounded off
	double xLeft = (double)xL;
	double xRight = (double)xR;

	//interpolated distance
	double d;

	for (int y = V2.y(); y > V0.y(); y--)
	{
		for (int x = xL; x <= xR; x++)
		{
			if (!insideCanvas(x, y)) continue;

			d = interpolateDistance(x, y, V0, V1, V2, d0, d1, d2);

			if (d < DepthBuffer[x][y])
			{
				DepthBuffer[x][y] = d;

				if (x == xL || x == xR) //black edges
				{
					painter.setPen(QPen(QColor::fromRgb(0, 0, 0), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));
				}
				else //surface colored interior
				{
					painter.setPen(QPen(surfaceBaseColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(surfaceBaseColor));
				}

				painter.drawPoint(x, y);
			}
		}
		xLeft += w0;
		xRight += w1;
		xL = ((int)(xLeft + 0.5));
		xR = ((int)(xRight + 0.5));
	}

	update();
}

void PaintWidget::upperTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor surfaceColor)
{
	QPainter painter(&image);

	double w0, w1; //reciprocal slopes

	w0 = ((double)(V0.x() - V2.x()) / (double)(V0.y() - V2.y()));
	w1 = ((double)(V1.x() - V2.x()) / (double)(V1.y() - V2.y()));


	int Ydiff = V2.y() - V0.y(); //how many rows to fill
								 //integer x-boundaries
	int xL = V0.x();
	int xR = V1.x();
	//these are the x-boundaries to be rounded off
	double xLeft = (double)xL;
	double xRight = (double)xR;

	//interpolated distance
	double d;

	for (int y = V0.y(); y <= V2.y(); y++)
	{
		for (int x = xL; x <= xR; x++)
		{
			if (!insideCanvas(x, y)) continue;

			d = interpolateDistance(x, y, V0, V1, V2, d0, d1, d2);

			if (d < DepthBuffer[x][y])
			{
				DepthBuffer[x][y] = d;

				if (x == xL || x == xR) //black edges
				{
					painter.setPen(QPen(QColor::fromRgb(0, 0, 0), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));
				}
				else //surface colored interior
				{
					painter.setPen(QPen(surfaceColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(surfaceColor));
				}

				painter.drawPoint(x, y);
			}
		}
		xLeft += w0;
		xRight += w1;
		xL = ((int)(xLeft + 0.5));
		xR = ((int)(xRight + 0.5));
	}

	update();
}

void PaintWidget::lowerTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor surfaceColor)
{
	QPainter painter(&image);

	double w0, w1; //reciprocal slopes

	w0 = ((double)(V0.x() - V2.x()) / (double)(V2.y() - V0.y()));
	w1 = ((double)(V1.x() - V0.x()) / (double)(V0.y() - V1.y()));

	int Ydiff = V2.y() - V0.y(); //how many rows to fill
								 //integer x-boundaries
	int xL = V2.x();
	int xR = V1.x();
	//these are the x-boundaries to be rounded off
	double xLeft = (double)xL;
	double xRight = (double)xR;

	//interpolated distance
	double d;

	for (int y = V2.y(); y > V0.y(); y--)
	{
		for (int x = xL; x <= xR; x++)
		{
			if (!insideCanvas(x, y)) continue;

			d = interpolateDistance(x, y, V0, V1, V2, d0, d1, d2);

			if (d < DepthBuffer[x][y])
			{
				DepthBuffer[x][y] = d;

				if (x == xL || x == xR) //black edges
				{
					painter.setPen(QPen(QColor::fromRgb(0, 0, 0), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));
				}
				else //surface colored interior
				{
					painter.setPen(QPen(surfaceColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(surfaceColor));
				}

				painter.drawPoint(x, y);
			}
		}
		xLeft += w0;
		xRight += w1;
		xL = ((int)(xLeft + 0.5));
		xR = ((int)(xRight + 0.5));
	}

	update();
}

void PaintWidget::upperTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor C0, QColor C1, QColor C2)
{
	QPainter painter(&image);
	QColor interpolatedColor;

	double w0, w1; //reciprocal slopes

	w0 = ((double)(V0.x() - V2.x()) / (double)(V0.y() - V2.y()));
	w1 = ((double)(V1.x() - V2.x()) / (double)(V1.y() - V2.y()));


	int Ydiff = V2.y() - V0.y(); //how many rows to fill
								 //integer x-boundaries
	int xL = V0.x();
	int xR = V1.x();
	//these are the x-boundaries to be rounded off
	double xLeft = (double)xL;
	double xRight = (double)xR;

	//interpolated distance
	double d;

	for (int y = V0.y(); y <= V2.y(); y++)
	{
		for (int x = xL; x <= xR; x++)
		{
			if (!insideCanvas(x, y)) continue;

			d = interpolateDistance(x, y, V0, V1, V2, d0, d1, d2);

			if (d < DepthBuffer[x][y])
			{
				DepthBuffer[x][y] = d;

				if (x == xL || x == xR) //black edges
				{
					painter.setPen(QPen(QColor::fromRgb(0, 0, 0), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));
				}
				else //surface colored interior
				{
					interpolatedColor = interpolateColor(x, y, V0, V1, V2, C0, C1, C2);
					painter.setPen(QPen(interpolatedColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(interpolatedColor));
				}

				painter.drawPoint(x, y);
			}
		}
		xLeft += w0;
		xRight += w1;
		xL = ((int)(xLeft + 0.5));
		xR = ((int)(xRight + 0.5));
	}

	update();
}

void PaintWidget::lowerTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor C0, QColor C1, QColor C2)
{
	QPainter painter(&image);
	QColor interpolatedColor;

	double w0, w1; //reciprocal slopes

	w0 = ((double)(V0.x() - V2.x()) / (double)(V2.y() - V0.y()));
	w1 = ((double)(V1.x() - V0.x()) / (double)(V0.y() - V1.y()));

	int Ydiff = V2.y() - V0.y(); //how many rows to fill
								 //integer x-boundaries
	int xL = V2.x();
	int xR = V1.x();
	//these are the x-boundaries to be rounded off
	double xLeft = (double)xL;
	double xRight = (double)xR;

	//interpolated distance
	double d;

	for (int y = V2.y(); y > V0.y(); y--)
	{
		for (int x = xL; x <= xR; x++)
		{
			if (!insideCanvas(x, y)) continue;

			d = interpolateDistance(x, y, V0, V1, V2, d0, d1, d2);

			if (d < DepthBuffer[x][y])
			{
				DepthBuffer[x][y] = d;

				if (x == xL || x == xR) //black edges
				{
					painter.setPen(QPen(QColor::fromRgb(0, 0, 0), 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(QColor::fromRgb(0, 0, 0)));
				}
				else //surface colored interior
				{
					interpolatedColor = interpolateColor(x, y, V0, V1, V2, C0, C1, C2);
					painter.setPen(QPen(interpolatedColor, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
					painter.setBrush(QBrush(interpolatedColor));
				}

				painter.drawPoint(x, y);
			}
		}
		xLeft += w0;
		xRight += w1;
		xL = ((int)(xLeft + 0.5));
		xR = ((int)(xRight + 0.5));
	}

	update();
}

int PaintWidget::RGBwithinRange(double value)
{
	int returnVal;
	if (value < 0.) returnVal = 0;
	else if (value > 255.) returnVal = 255;
	else returnVal = ((int)(value + 0.5));

	return returnVal;
}

QColor PaintWidget::computeColor(PointVect *ptOfIncidence, PointVect *normal)
{
	int R, G, B;
	double specularR, specularG, specularB;
	double diffuseR, diffuseG, diffuseB;
	double redLightComponent, greenLightComponent, blueLightComponent;
	double scalar1, scalar2, prod1;

	PointVect reflectionVector;
	PointVect lightVector;
	PointVect observerVector = ourCamera.ObserverVectorFrom(*ptOfIncidence);
	
	//make the surfaceBaseColor a little darker
	R = (int)(0.5 * (float)surfaceBaseColor.red() + 0.5);
	G = (int)(0.5 * (float)surfaceBaseColor.green() + 0.5);
	B = (int)(0.5 * (float)surfaceBaseColor.blue() + 0.5);

	//R = surfaceBaseColor.red();
	//G = surfaceBaseColor.green();
	//B = surfaceBaseColor.blue();

	//add the ambient component
	R += Ambient.red();
	G += Ambient.green();
	B += Ambient.blue();

	int NLights = Lights.size();

	for (int i = 0; i < NLights; i++)
	{
		reflectionVector = ReflectionVector(Lights[i], *ptOfIncidence, *normal);
		normalize(reflectionVector);
		lightVector = Lights[i].PositionFrom(ptOfIncidence);
		normalize(lightVector);
		prod1 = dot(observerVector, reflectionVector);
		scalar2 = dot(lightVector, *normal);

		//specular component
		if (scalar2 < 0. || prod1 < 0.)
		{
			scalar1 = 0.;
			specularR = 0.; specularG = 0.; specularB = 0.;
		}
		else 
		{
			scalar1 = pow(prod1, shininess);
			specularR = Specular.red() * scalar1;
			specularG = Specular.green() * scalar1;
			specularB = Specular.blue() * scalar1;
		}

		//diffuse component
		if (scalar2 < 0.)
		{
			diffuseR = 0.; diffuseG = 0.; diffuseB = 0.;
		}
		else
		{
			diffuseR = Diffusion.red() * scalar2;
			diffuseG = Diffusion.green() * scalar2;
			diffuseB = Diffusion.blue() * scalar2;
		}

		redLightComponent = specularR + diffuseR;
		greenLightComponent = specularG + diffuseG;
		blueLightComponent = specularB + diffuseB;

		R += RGBwithinRange(redLightComponent);
		G += RGBwithinRange(greenLightComponent);
		B += RGBwithinRange(blueLightComponent);
	}


	sColor = QColor::fromRgb(RGBwithinRange((double)R), RGBwithinRange((double)G), RGBwithinRange((double)B));

	/*
	if ((R < G + 5 && R > G - 5) && (G < B + 5 && G > B - 5) && (R > 150 && G > 150 && B > 150) && prod1 < DBL_MIN)
	{
		//debugging stuff
		printf("----------------Surface[%d]-------------------\n", surfaceIndex);
		printf("Surface Normal : (%lf, %lf, %lf)\n", surfaceNormal.x(), surfaceNormal.y(), surfaceNormal.z());
		printf("Surface center : (%lf, %lf, %lf)\n", surfaceCenter.x(), surfaceCenter.y(), surfaceCenter.z());
		printf("--------------- arguments -------------------\n");
		printf("Normal : (%lf, %lf, %lf)\n", normal->x(), normal->y(), normal->z());
		printf("center : (%lf, %lf, %lf)\n", ptOfIncidence->x(), ptOfIncidence->y(), ptOfIncidence->z());
		printf("----------- calculating clolor --------------\n");
		printf("(R0, G0, B0) = (%d, %d, %d)\n", surfaceBaseColor.red(), surfaceBaseColor.green(), surfaceBaseColor.blue());
		printf("(redLightComponent, greenLightComponent, blueLightComponent) = (%d, %d, %d)\n", redLightComponent, greenLightComponent, blueLightComponent);
		printf("lightPosition: (%lf, %lf, %lf)\n", Lights[0].Position().x(), Lights[0].Position().y(), Lights[0].Position().z());
		printf("observerVector: (%lf, %lf, %lf)\n", observerVector.x(), observerVector.y(), observerVector.z());
		printf("reflectionVector: (%lf, %lf, %lf)\n", reflectionVector.x(), reflectionVector.y(), reflectionVector.z());
		printf("lightVector: (%lf, %lf, %lf)\n", lightVector.x(), lightVector.y(), lightVector.z());
		printf("prod1 = %lf\n", prod1);
		printf("scalar1 = %lf\n", scalar1);
		printf("scalar2 = %lf\n", scalar2);
		printf("(R, G, B) = (%d, %d, %d)\n", R, G, B);
		printf("\n");
	}*/

	return sColor;
}

//for sorting vertices with associated distances
typedef struct {
	QPoint point;
	double distance;
} QPointWithDistance;

//for sorting vertices with associated distnances and normals
typedef struct {
	QPoint point;
	double distance;
	QColor color;
} QPointWithDistAndColor;

bool cmp_QPointWd_Y(const QPointWithDistance &P0, const QPointWithDistance &P1)
{
	return (P0.point.y() < P1.point.y());
}

bool cmp_QPointWdc_Y(const QPointWithDistAndColor &P0, const QPointWithDistAndColor &P1)
{
	return (P0.point.y() < P1.point.y());
}

//no shading version
void PaintWidget::PixelTriangleScanline(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2)
{
	QPoint v0, v1, v2; //re-ordered vertices
	double dd0, dd1, dd2, d3; //re-ordered vertex x-distances from the projection plane (for the distance-buffer)
	QPointWithDistance Qd;

	//is one edge horizontal?
	//if so does the triangle point up or down?
	if (V0.y() == V1.y()) // V2 remaining
	{
		if (V2.y() > V0.y() && V2.y() > V1.y()) //points up
		{
			v0 = (V0.x() < V1.x() ? V0 : V1);
			dd0 = (V0.x() < V1.x() ? d0 : d1);
			v1 = (V0.x() < V1.x() ? V1 : V0);
			dd1 = (V0.x() < V1.x() ? d1 : d0);
			v2 = V2;
			dd2 = d2;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V2.y() < V0.y() && V2.y() < V1.y()) //points down
		{
			v0 = V2;
			dd0 = d2;
			v1 = (V0.x() < V1.x() ? V1 : V0);
			dd1 = (V0.x() < V1.x() ? d1 : d0);
			v2 = (V0.x() < V1.x() ? V0 : V1);
			dd2 = (V0.x() < V1.x() ? d0 : d1);
			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else if (V0.y() == V2.y()) // V1 remaining
	{
		if (V1.y() > V0.y() && V1.y() > V2.y()) //points up
		{
			v0 = (V0.x() < V2.x() ? V0 : V2);
			dd0 = (V0.x() < V2.x() ? d0 : d2);
			v1 = (V0.x() < V2.x() ? V2 : V0);
			dd1 = (V0.x() < V2.x() ? d2 : d0);
			v2 = V1;
			dd2 = d1;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V1.y() < V0.y() && V1.y() < V2.y()) //points down
		{
			v0 = V1;
			dd0 = d1;
			v1 = (V0.x() < V2.x() ? V2 : V0);
			dd1 = (V0.x() < V2.x() ? d2 : d0);
			v2 = (V0.x() < V2.x() ? V0 : V2);
			dd2 = (V0.x() < V2.x() ? d0 : d2);
			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else if (V2.y() == V1.y()) // V0 remaining
	{
		if (V0.y() > V2.y() && V0.y() > V1.y()) //points up
		{
			v0 = (V2.x() < V1.x() ? V2 : V1);
			dd0 = (V2.x() < V1.x() ? d2 : d1);
			v1 = (V2.x() < V1.x() ? V1 : V2);
			dd1 = (V2.x() < V1.x() ? d1 : d2);
			v2 = V0;
			dd2 = d0;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V0.y() < V2.y() && V0.y() < V1.y()) //points down
		{
			v0 = V0;
			dd0 = d0;
			v1 = (V2.x() < V1.x() ? V1 : V2);
			dd1 = (V2.x() < V1.x() ? d1 : d2);
			v2 = (V2.x() < V1.x() ? V2 : V1);
			dd2 = (V2.x() < V1.x() ? d2 : d1);
			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else
	{
		std::vector<QPointWithDistance> Vertices;
		Qd.point = V0; Qd.distance = d0;
		Vertices.push_back(Qd);

		Qd.point = V1; Qd.distance = d1;
		Vertices.push_back(Qd);

		Qd.point = V2; Qd.distance = d2;
		Vertices.push_back(Qd);

		//sort by y-coord, lowest to highest
		qSort(Vertices.begin(), Vertices.end(), cmp_QPointWd_Y);

		if (!(Vertices[0].point.y() < Vertices[1].point.y() && Vertices[1].point.y() < Vertices[2].point.y()))
		{
			printf("ERROR! BAD SORTING!");
		}

		//real-valued parameter lambda from [0,1] is the convex combination parameter of points Vertices[0] and Vertices[2]
		double lambda = ((double)(Vertices[1].point.y() - Vertices[2].point.y())) / ((double)(Vertices[0].point.y() - Vertices[2].point.y()));
		//this is the midpoint
		QPoint V3(((int)((1 - lambda) * Vertices[2].point.x() + lambda * Vertices[0].point.x() + 0.5)), Vertices[1].point.y());

		//interpolated distance of the third point
		d3 = (1 - lambda) * Vertices[2].distance + lambda * Vertices[0].distance;

		v0 = (Vertices[1].point.x() < V3.x() ? Vertices[1].point : V3);
		dd0 = (Vertices[1].point.x() < V3.x() ? Vertices[1].distance : d3);
		v1 = (Vertices[1].point.x() < V3.x() ? V3 : Vertices[1].point);
		dd1 = (Vertices[1].point.x() < V3.x() ? d3 : Vertices[1].distance);
		v2 = Vertices[2].point;
		dd2 = Vertices[2].distance;
		upperTriangleFill(v0, v1, v2, dd0, dd1, dd2);

		v0 = Vertices[0].point;
		dd0 = Vertices[0].distance;
		v1 = (Vertices[1].point.x() < V3.x() ? V3 : Vertices[1].point);
		dd1 = (Vertices[1].point.x() < V3.x() ? d3 : Vertices[1].distance);
		v2 = (Vertices[1].point.x() < V3.x() ? Vertices[1].point : V3);
		dd2 = (Vertices[1].point.x() < V3.x() ? Vertices[1].distance : d3);
		lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2);
	}
}

//flat shading version
void PaintWidget::PixelTriangleScanline(PointVect *surfNormal, PointVect *center, QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2)
{
	QPoint v0, v1, v2; //re-ordered vertices
	double dd0, dd1, dd2, d3; //re-ordered vertex x-distances from the projection plane (for the distance-buffer)
	QPointWithDistance Qd;
	//surface color computed by a flat shading algorithm (Phong)
	QColor surfaceColor = computeColor(center, surfNormal);

	//is one edge horizontal?
	//if so does the triangle point up or down?
	if (V0.y() == V1.y()) // V2 remaining
	{
		if (V2.y() > V0.y() && V2.y() > V1.y()) //points up
		{
			v0 = (V0.x() < V1.x() ? V0 : V1);
			dd0 = (V0.x() < V1.x() ? d0 : d1);
			v1 = (V0.x() < V1.x() ? V1 : V0);
			dd1 = (V0.x() < V1.x() ? d1 : d0);
			v2 = V2;
			dd2 = d2;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V2.y() < V0.y() && V2.y() < V1.y()) //points down
		{
			v0 = V2;
			dd0 = d2;
			v1 = (V0.x() < V1.x() ? V1 : V0);
			dd1 = (V0.x() < V1.x() ? d1 : d0);
			v2 = (V0.x() < V1.x() ? V0 : V1);
			dd2 = (V0.x() < V1.x() ? d0 : d1);
			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else if (V0.y() == V2.y()) // V1 remaining
	{
		if (V1.y() > V0.y() && V1.y() > V2.y()) //points up
		{
			v0 = (V0.x() < V2.x() ? V0 : V2);
			dd0 = (V0.x() < V2.x() ? d0 : d2);
			v1 = (V0.x() < V2.x() ? V2 : V0);
			dd1 = (V0.x() < V2.x() ? d2 : d0);
			v2 = V1;
			dd2 = d1;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V1.y() < V0.y() && V1.y() < V2.y()) //points down
		{
			v0 = V1;
			dd0 = d1;
			v1 = (V0.x() < V2.x() ? V2 : V0);
			dd1 = (V0.x() < V2.x() ? d2 : d0);
			v2 = (V0.x() < V2.x() ? V0 : V2);
			dd2 = (V0.x() < V2.x() ? d0 : d2);
			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else if (V2.y() == V1.y()) // V0 remaining
	{
		if (V0.y() > V2.y() && V0.y() > V1.y()) //points up
		{
			v0 = (V2.x() < V1.x() ? V2 : V1);
			dd0 = (V2.x() < V1.x() ? d2 : d1);
			v1 = (V2.x() < V1.x() ? V1 : V2);
			dd1 = (V2.x() < V1.x() ? d1 : d2);
			v2 = V0;
			dd2 = d0;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V0.y() < V2.y() && V0.y() < V1.y()) //points down
		{
			v0 = V0;
			dd0 = d0;
			v1 = (V2.x() < V1.x() ? V1 : V2);
			dd1 = (V2.x() < V1.x() ? d1 : d2);
			v2 = (V2.x() < V1.x() ? V2 : V1);
			dd2 = (V2.x() < V1.x() ? d2 : d1);
			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else
	{
		std::vector<QPointWithDistance> Vertices;
		Qd.point = V0; Qd.distance = d0;
		Vertices.push_back(Qd);

		Qd.point = V1; Qd.distance = d1;
		Vertices.push_back(Qd);

		Qd.point = V2; Qd.distance = d2;
		Vertices.push_back(Qd);

		//sort by y-coord, lowest to highest
		qSort(Vertices.begin(), Vertices.end(), cmp_QPointWd_Y);

		if (!(Vertices[0].point.y() < Vertices[1].point.y() && Vertices[1].point.y() < Vertices[2].point.y()))
		{
			printf("ERROR! BAD SORTING!");
		}

		//real-valued parameter lambda from [0,1] is the convex combination parameter of points Vertices[0] and Vertices[2]
		double lambda = ((double)(Vertices[1].point.y() - Vertices[2].point.y())) / ((double)(Vertices[0].point.y() - Vertices[2].point.y()));
		//this is the midpoint
		QPoint V3(((int)((1 - lambda) * Vertices[2].point.x() + lambda * Vertices[0].point.x() + 0.5)), Vertices[1].point.y());

		//interpolated distance of the third point
		d3 = (1 - lambda) * Vertices[2].distance + lambda * Vertices[0].distance;

		v0 = (Vertices[1].point.x() < V3.x() ? Vertices[1].point : V3);
		dd0 = (Vertices[1].point.x() < V3.x() ? Vertices[1].distance : d3);
		v1 = (Vertices[1].point.x() < V3.x() ? V3 : Vertices[1].point);
		dd1 = (Vertices[1].point.x() < V3.x() ? d3 : Vertices[1].distance);
		v2 = Vertices[2].point;
		dd2 = Vertices[2].distance;
		upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);

		v0 = Vertices[0].point;
		dd0 = Vertices[0].distance;
		v1 = (Vertices[1].point.x() < V3.x() ? V3 : Vertices[1].point);
		dd1 = (Vertices[1].point.x() < V3.x() ? d3 : Vertices[1].distance);
		v2 = (Vertices[1].point.x() < V3.x() ? Vertices[1].point : V3);
		dd2 = (Vertices[1].point.x() < V3.x() ? Vertices[1].distance : d3);
		lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, surfaceColor);
	}
}

//Gouraud shading version
void PaintWidget::PixelTriangleScanline(PointVect *p0, PointVect *p1, PointVect *p2, PointVect *n0, PointVect *n1, PointVect *n2, QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2)
{
	QPoint v0, v1, v2; //re-ordered vertices
	double dd0, dd1, dd2, d3; //re-ordered vertex x-distances from the projection plane (for the distance-buffer)
	QPointWithDistAndColor Qdc;
	//computed colors of individual vertices which will later be interpolated for every point inside the triangle
	surfaceNormal = *n0; surfaceCenter = *p0;
	QColor C0 = computeColor(p0, n0);
	surfaceNormal = *n1; surfaceCenter = *p1;
	QColor C1 = computeColor(p1, n1);
	surfaceNormal = *n2; surfaceCenter = *p2;
	QColor C2 = computeColor(p2, n2);
	QColor C3; //interpolated color for midpoint
	QColor cc0, cc1, cc2; //re-ordered vertex colors

	//is one edge horizontal?
	//if so does the triangle point up or down?
	if (V0.y() == V1.y()) // V2 remaining
	{
		if (V2.y() > V0.y() && V2.y() > V1.y()) //points up
		{
			v0 = (V0.x() < V1.x() ? V0 : V1);
			dd0 = (V0.x() < V1.x() ? d0 : d1);
			cc0 = (V0.x() < V1.x() ? C0 : C1);

			v1 = (V0.x() < V1.x() ? V1 : V0);
			dd1 = (V0.x() < V1.x() ? d1 : d0);
			cc1 = (V0.x() < V1.x() ? C1 : C0);

			v2 = V2;
			dd2 = d2;
			cc2 = C2;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V2.y() < V0.y() && V2.y() < V1.y()) //points down
		{
			v0 = V2;
			dd0 = d2;
			cc0 = C2;

			v1 = (V0.x() < V1.x() ? V1 : V0);
			dd1 = (V0.x() < V1.x() ? d1 : d0);
			cc1 = (V0.x() < V1.x() ? C1 : C0);

			v2 = (V0.x() < V1.x() ? V0 : V1);
			dd2 = (V0.x() < V1.x() ? d0 : d1);
			cc2 = (V0.x() < V1.x() ? C0 : C1);

			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else if (V0.y() == V2.y()) // V1 remaining
	{
		if (V1.y() > V0.y() && V1.y() > V2.y()) //points up
		{
			v0 = (V0.x() < V2.x() ? V0 : V2);
			dd0 = (V0.x() < V2.x() ? d0 : d2);
			cc0 = (V0.x() < V2.x() ? C0 : C2);

			v1 = (V0.x() < V2.x() ? V2 : V0);
			dd1 = (V0.x() < V2.x() ? d2 : d0);
			cc1 = (V0.x() < V2.x() ? C2 : C0);

			v2 = V1;
			dd2 = d1;
			cc2 = C1;
			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V1.y() < V0.y() && V1.y() < V2.y()) //points down
		{
			v0 = V1;
			dd0 = d1;
			cc0 = C1;

			v1 = (V0.x() < V2.x() ? V2 : V0);
			dd1 = (V0.x() < V2.x() ? d2 : d0);
			cc1 = (V0.x() < V2.x() ? C2 : C0);

			v2 = (V0.x() < V2.x() ? V0 : V2);
			dd2 = (V0.x() < V2.x() ? d0 : d2);
			cc2 = (V0.x() < V2.x() ? C0 : C2);

			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else if (V2.y() == V1.y()) // V0 remaining
	{
		if (V0.y() > V2.y() && V0.y() > V1.y()) //points up
		{
			v0 = (V2.x() < V1.x() ? V2 : V1);
			dd0 = (V2.x() < V1.x() ? d2 : d1);
			cc0 = (V2.x() < V1.x() ? C2 : C1);

			v1 = (V2.x() < V1.x() ? V1 : V2);
			dd1 = (V2.x() < V1.x() ? d1 : d2);
			cc1 = (V2.x() < V1.x() ? C1 : C2);

			v2 = V0;
			dd2 = d0;
			cc2 = C0;

			upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);
			bufferBetweenPts(v0, v1, dd0, dd1);
		}
		else if (V0.y() < V2.y() && V0.y() < V1.y()) //points down
		{
			v0 = V0;
			dd0 = d0;
			cc0 = C0;

			v1 = (V2.x() < V1.x() ? V1 : V2);
			dd1 = (V2.x() < V1.x() ? d1 : d2);
			cc1 = (V2.x() < V1.x() ? C1 : C2);

			v2 = (V2.x() < V1.x() ? V2 : V1);
			dd2 = (V2.x() < V1.x() ? d2 : d1);
			cc2 = (V2.x() < V1.x() ? C2 : C1);

			lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);
			bufferBetweenPts(v2, v1, dd2, dd1);
		}
		else //zero area triangle
		{
			//printf("ERROR! Triangle with zero area!\n");
		}
	}
	else
	{
		std::vector<QPointWithDistAndColor> Vertices;
		Qdc.point = V0; Qdc.distance = d0; Qdc.color = C0;
		Vertices.push_back(Qdc);

		Qdc.point = V1; Qdc.distance = d1; Qdc.color = C1;
		Vertices.push_back(Qdc);

		Qdc.point = V2; Qdc.distance = d2; Qdc.color = C2;
		Vertices.push_back(Qdc);

		//sort by y-coord, lowest to highest
		qSort(Vertices.begin(), Vertices.end(), cmp_QPointWdc_Y);

		if (!(Vertices[0].point.y() < Vertices[1].point.y() && Vertices[1].point.y() < Vertices[2].point.y()))
		{
			printf("ERROR! BAD SORTING!");
		}

		//real-valued parameter lambda from [0,1] is the convex combination parameter of points Vertices[0] and Vertices[2]
		double lambda = ((double)(Vertices[1].point.y() - Vertices[2].point.y())) / ((double)(Vertices[0].point.y() - Vertices[2].point.y()));
		//this is the midpoint
		QPoint V3(((int)((1 - lambda) * Vertices[2].point.x() + lambda * Vertices[0].point.x() + 0.5)), Vertices[1].point.y());

		//interpolated distance of the third point
		d3 = (1 - lambda) * Vertices[2].distance + lambda * Vertices[0].distance;
		//interpolated color for the third point
		C3 = QColor::fromRgb((int)((1 - lambda) * (double)Vertices[2].color.red() + lambda * (double)Vertices[0].color.red()),
							 (int)((1 - lambda) * (double)Vertices[2].color.green() + lambda * (double)Vertices[0].color.green()),
							 (int)((1 - lambda) * (double)Vertices[2].color.blue() + lambda * (double)Vertices[0].color.blue()));


		v0 = (Vertices[1].point.x() < V3.x() ? Vertices[1].point : V3);
		dd0 = (Vertices[1].point.x() < V3.x() ? Vertices[1].distance : d3);
		cc0 = (Vertices[1].point.x() < V3.x() ? Vertices[1].color : C3);

		v1 = (Vertices[1].point.x() < V3.x() ? V3 : Vertices[1].point);
		dd1 = (Vertices[1].point.x() < V3.x() ? d3 : Vertices[1].distance);
		cc1 = (Vertices[1].point.x() < V3.x() ? C3 : Vertices[1].color);

		v2 = Vertices[2].point;
		dd2 = Vertices[2].distance;
		cc2 = Vertices[2].color;

		upperTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);

		v0 = Vertices[0].point;
		dd0 = Vertices[0].distance;
		cc0 = Vertices[0].color;

		v1 = (Vertices[1].point.x() < V3.x() ? V3 : Vertices[1].point);
		dd1 = (Vertices[1].point.x() < V3.x() ? d3 : Vertices[1].distance);
		cc1 = (Vertices[1].point.x() < V3.x() ? C3 : Vertices[1].color);

		v2 = (Vertices[1].point.x() < V3.x() ? Vertices[1].point : V3);
		dd2 = (Vertices[1].point.x() < V3.x() ? Vertices[1].distance : d3);
		cc2 = (Vertices[1].point.x() < V3.x() ? Vertices[1].color : C3);

		lowerTriangleFill(v0, v1, v2, dd0, dd1, dd2, cc0, cc1, cc2);
	}	
}

void PaintWidget::PolygonTransformScale(int N, int value)
{
	clearImage();

	printf("scalefactor = %.4lf\n", (((double)(value + 100)) / 100.));
	
	for (int i = 1; i < N; i++)
	{
		scaledX = (((double)(value + 100)) / 100.)*( vertices.at(i).x() - fpointX ) + fpointX + 0.5;
		scaledY = (((double)(value + 100)) / 100.)*( vertices.at(i).y() - fpointY ) + fpointY + 0.5;

		vertices.at(i).setX((int)scaledX);
		vertices.at(i).setY((int)scaledY);
	}

	PolygonScanline(vertices.data(), N, myPenColor);
	update();
}

void PaintWidget::PolygonTransformRotate(int N,int value)
{
	clearImage();
	radian = ((double)value)/(360.)*2*M_PI;

	printf("angle = %.4lf\n", radian);

	for (int i = 1; i < N; i++)
	{
		rotatedX = cos(radian)*(vertices.at(i).x() - fpointX) - sin(radian)*(vertices.at(i).y() - fpointY) + fpointX + 0.5;
		rotatedY = sin(radian)*(vertices.at(i).x() - fpointX) + cos(radian)*(vertices.at(i).y() - fpointY) + fpointY + 0.5;
		
		vertices.at(i).setX((int)rotatedX);
		vertices.at(i).setY((int)rotatedY);
	}

	PolygonScanline(vertices.data(), N, myPenColor);
	update();
}

void PaintWidget::sceneInit()
{
	PointVect cameraPosition(CameraDistance, 0., 0.);
	ourCamera = Camera(cameraPosition, PlaneDistance);

	PointVect lightPos = PointVect(10., 0., 0.);
	QColor lightColor = QColor::fromRgb(255, 255, 255);
	Lights.push_back(Light(lightPos, lightColor));

	PointVect cameraLightPos = ourCamera.GlobalToCameraCoord(lightPos);

	printf("lightPos = (%lf, %lf, %lf)\n", lightPos.x(), lightPos.y(), lightPos.z());
	printf("cameraLightPos = (%lf, %lf, %lf)\n", cameraLightPos.x(), cameraLightPos.y(), cameraLightPos.z());
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	QPoint *coords;
	int S;
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		if ( vertex_draw == true && N > 0 )
		{
			vertices.push_back(lastPoint);
			printf("size (vertices) = %d .... remaining : %d \n", (int) vertices.size(),N);
			N--;
			if (N == 0)
			{
				vertex_draw = false;
				
				printf("vertex_draw = false\n");
				coords = vertices.data();
				S = (int) vertices.size();


				for (int i = 0; i < S - 1 ; i++)
				{
					DDALine(QPoint((coords + i)->x(), (coords + i)->y()), QPoint((coords + i + 1)->x(), (coords + i + 1)->y()), 0, 0, 0, 5);
				}

				DDALine(QPoint(coords->x(), coords->y()), QPoint((coords + S - 1)->x(), (coords + S - 1)->y()), 0, 0, 0, 5);

				PolygonScanline(coords, S, myPenColor);						
			}
		}

		if (move == true && scale == false && rotate == false)
		{
			StartingPt = lastPoint;
			printf("StartingPt=(%d,%d)\n", StartingPt.x(), StartingPt.y());
		}
	}
	update();
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if (move == true && scale == false && rotate == false)
	{
		EndingPt = event->pos();
		PolygonTransformMove(vertices.size());
		printf("EndingPt=(%d,%d)\n", EndingPt.x(), EndingPt.y());
		StartingPt = EndingPt;
	}

	update();
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}


