#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QElapsedTimer>

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();

public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void ActionNegative();
	void ActionBlackWhite();
	void ActionSepiaTone();
	void ActionMedian();
	void ActionSaltPepper();
	void ActionLeft();
	void ActionRight();

	void ActionValueChanged();
	void ActionGenerateSphere();
	void ActionProjectPerspective();
	void ActionProjectParallel();

	void ActionViewMode_PointCloud();
	void ActionViewMode_Wireframe();
	void ActionViewMode_Surfaces();

	void ActionEnableCameraRotation();

	void ActionShadingFlat();
	void ActionShadingGouraud();

	void ActionAddLight();
	void ActionRemoveLastLight();
	void ActionUpdateLightColors();
	void ActionClearLights();

	void ActionGenerateBezier();
	void ActionLoadBezier();
	void ActionAutoBezier();
	void ActionAddControlPoint();
	void ActionRemoveLastControlPoint();
	void ActionSwitchDefaultBezier();

	void UpdateControlPts();

	void ActionDisplayBezierControlPts();

private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;
};

#endif // MYPAINTER_H
