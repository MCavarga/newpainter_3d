#include "Edgy.h"
#include <algorithm>

Edgy::Edgy()
{
}


Edgy::~Edgy()
{
}

Edgy::Edgy(int x0, int y0, int x1, int y1)
{
   this->x0 = x0;
   this->y0 = y0; 
   this->x1 = x1; 
   this->y1 = y1;
}

void Edgy::SetW(double w)
{
	this->w = w;
}

int Edgy::X0() const
{
	return x0;
}

int Edgy::Y0() const
{
	return y0;
}

int Edgy::X1() const
{
	return x1;
}

int Edgy::Y1() const
{
	return y1;
}

int Edgy::XL() const
{
	if (x0 < x1)
	{
		return x0;
	}
	else
	{
		return x1;
	}
}

double Edgy::W() const
{
	return w;
}

void Edgy::swap_pts()
{

	if (x0 > x1 && y0 < y1 )
	{
		std::swap(x0, x1);
		std::swap(y0, y1);
	}
	else if (x0 < x1 && y0 < y1)
	{
		std::swap(x0, x1);
		std::swap(y0, y1);
	}
	else if (x0 == x1 && y0 < y1)
	{
		std::swap(y0, y1);
	}
}

