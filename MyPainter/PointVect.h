#pragma once

class PointVect
{
public:
	PointVect();
	PointVect(double xx, double yy, double zz);
	~PointVect();
	void setX(double xx) { this->xx = xx; };
	void setY(double yy) { this->yy = yy; };
	void setZ(double zz) { this->zz = zz; };
	void setIndex(int index) { this->index = index; };
	void setNx(double nx) { this->nx = nx; };
	void setNy(double ny) { this->ny = ny; };
	void setNz(double nz) { this->nz = nz; };
	double x() const { return xx; };
	double y() const { return yy; };
	double z() const { return zz; };
	int idx() const { return index; };
	double Nx() const { return nx; };
	double Ny() const { return ny; };
	double Nz() const { return nz; };
private:
	double xx;
	double yy;
	double zz;
	int index;
	//vertex normal coordinates if this is the vertex of a surface
	double nx;
	double ny;
	double nz;
};

