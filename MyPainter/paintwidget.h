#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QColorDialog>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <PointVect.h>
#include <EdgeVect.h>
#include <Triangle.h>

class Light
{
private:
	PointVect position;
	QColor color;
public:
	Light(PointVect pos, QColor lcolor) { position = pos; color = lcolor; }
	~Light() { }
	QColor Color() { return color; }
	PointVect Position() { return position; }
	PointVect PositionFrom(PointVect *point) // returns the vector of the incoming light ray onto a given point
	{
		PointVect result;
		result.setX(position.x() - point->x());
		result.setY(position.y() - point->y());
		result.setZ(position.z() - point->z());

		return result;
	}
	void setRGB(int R, int G, int B)
	{
		if (R >= 0 && R <= 255)
		{
			color.setRed(R);
		}
		else if (R < 0)
		{
			color.setRed(0);
		}
		else
		{
			color.setRed(255);
		}

		if (G >= 0 && G <= 255)
		{
			color.setGreen(G);
		}
		else if (G < 0)
		{
			color.setGreen(0);
		}
		else
		{
			color.setGreen(255);
		}

		if (B >= 0 && B <= 255)
		{
			color.setBlue(B);
		}
		else if (B < 0)
		{
			color.setBlue(0);
		}
		else
		{
			color.setBlue(255);
		}
	}
	void updatePosition(float x, float y, float z)
	{
		position.setX(x); position.setY(y); position.setZ(z);
	}
};

// ------------------------------
// ---- camera stuff ------------
class Camera
{
private:
	PointVect focusPosition; //origin of the camera coordinate system
	double cameraDistance; //global origin to focusPosition
	double planeDistance; //focusPosition to the projection plane (perpendicular)
public:
	Camera() { }
	~Camera() { }
	Camera(PointVect pos, double d)
	{
		focusPosition = pos; cameraDistance = norm(pos); planeDistance = d;
	}
	PointVect ObserverVectorFrom(PointVect X) //the argument has to be a vector in global coordinates (used for shading)
	{
		PointVect result = PointVect(focusPosition.x() - X.x(), focusPosition.y() - X.y(), focusPosition.z() - X.z());
		normalize(result);
		return result;
	}
	void PlaneZoom(double newDistance)
	{
		PointVect normalizedFocus = focusPosition;
		normalize(normalizedFocus);
		focusPosition.setX(newDistance * normalizedFocus.x());
		focusPosition.setY(newDistance * normalizedFocus.y());
		focusPosition.setZ(newDistance * normalizedFocus.z());
	}
	void setFieldOfView(double d)
	{
		planeDistance = d;
	}
	void RotateCamera(double phi, double theta)
	{
		double rotatedX, rotatedY, rotatedZ;

		rotatedX = cos(theta) * (cos(phi) * focusPosition.x() - sin(phi) * focusPosition.y()) - sin(theta) * focusPosition.z();
		rotatedY = sin(phi) * focusPosition.x() + cos(phi) * focusPosition.y();
		rotatedZ = sin(theta) * (cos(phi) * focusPosition.x() - sin(phi) * focusPosition.y()) + cos(theta) * focusPosition.z();

		focusPosition.setX(rotatedX); focusPosition.setY(rotatedY); focusPosition.setZ(rotatedZ);		
	}
	PointVect FocusPosition() { return focusPosition; }
	PointVect GlobalToCameraCoord(PointVect X)
	{
		// if the camera coordiante system has the same spatial orientation as the global system, 
		// the resulting vector is: focusPosition - X. However, we need to take orientation into account,
		// so that the camera would always look at the origin of the global coordinate system.
		// I want to rotate the camera coordinate system x', y', z' with origin at focusPosition so that
		// z'-axis coincides with focusPosition, that is: focusPosition' = (0, 0, cameraDistance), and 
		// I want the x'-axis be parallel to the xy plane of the global system, with the y'-axis 
		// pointing towards the global z-axis for 0 <= theta <= pi.
		// 
		double r = cameraDistance; //z' coordinate of the origin
		PointVect translatedX = PointVect(focusPosition.x() - X.x(), focusPosition.y() - X.y(), focusPosition.z() - X.z());
		double focusX = focusPosition.x();
		double phi = atan2(focusPosition.y(), focusX); //azimuthal angle of the camera

		// KNOWN BUG! when we rotate by theta past pi/2 this formula thinks its before pi/2 not after
		double theta = asin(focusPosition.z() / r); //zenith angle of the camera
		/*
		if (focusPosition.z() < 0.)
		{
			printf("focusPosition.z() = %lf , theta = %lf\n", focusPosition.z(), theta);
		}	*/	

		PointVect newZnormal(focusPosition.x() / r, focusPosition.y() / r, focusPosition.z() / r); //points in the new z'-direction
		//focusPosition's orthoprojection onto the xy-plane
		double rho = sqrt(focusPosition.x() * focusPosition.x() + focusPosition.y() * focusPosition.y());

		//points in the new x'-direction, coinciding with the tangent to the phi-orbit circle
		PointVect newXnormal = PointVect(-rho * sin(phi), rho * cos(phi), 0.);

		//points in the new y'-direction coinciding with the tangent to the theta-orbit circle
		PointVect newYnormal = cross(newXnormal, newZnormal);

		//these are essentially orthogonal distances of translatedX to three different planes 
		//the normals of which coincide with x',y', and z'-axes
		double newX = dot(newXnormal, translatedX) / norm(newXnormal);
		double newY = dot(newYnormal, translatedX) / norm(newYnormal);
		double newZ = dot(newZnormal, translatedX) / norm(newZnormal);

		return PointVect(newX, newY, newZ);
	}
	double DistanceToPlaneCamera(PointVect X) //the argument has to be a point in camera coordinates
	{
		return X.z();
	}
	double GlobalDistanceToFocus(PointVect X) //the argument has to be a point in global coordinates
	{
		return norm(PointVect(focusPosition.x() - X.x(), focusPosition.y() - X.y(), focusPosition.y() - X.y()));
	}
	double CameraDistanceToFocus(PointVect X) //the argument has to be a point in camera coordinates
	{
		return norm(X);
	}
	QPoint CameraToCanvasCoord(PointVect X, int width, int height, bool centered) //the argument has to be a point in camera coordinates
	{
		double originX = 0.5 * width;
		double originY = 0.5 * height;

		double projX, projY;

		if (centered) //perspective projection
		{
			//the projected coordinates are calculated using the similarity of triangles formed by the focalPosition
			//that is: the origin of the camera coordinates, point X (in camera coordinates), 
			//and the origin of the global coordinate system written in camera coordinates, all while
			//making sure that a unit sphere fits into the frame, having decided to scale the projected image to a quarter of the image width

			double ptDistance = CameraDistanceToFocus(X);

			projX = 0.25 * width * cameraDistance * planeDistance * X.x() / ptDistance;
			projY = -0.25 * width * cameraDistance * planeDistance * X.y() / ptDistance;
			return QPoint(((int)(projX + originX + 0.5)), ((int)(projY + originY + 0.5)));
		}
		else //parallel projection
		{
			//the projected coordinates are just extracted from the camera y-coord and inverted camera z-coord respectively, that is
			//in the direction of the plane (camera) unit normal (1, 0, 0), all while making sure that a unit sphere fits into the frame,
			//having decided to scale the projected image to a quarter of the image width
			projX = 0.25 * width * X.x();
			projY = -0.25 * width * X.y();
			return QPoint(((int)(projX + originX + 0.5)), ((int)(projY + originY + 0.5)));
		}
	}
	// ----- vector stuff -----
	double dot(PointVect P0, PointVect P1)
	{
		return (P0.x() * P1.x() + P0.y() * P1.y() + P0.z() * P1.z());
	}

	double norm(PointVect P)
	{
		return sqrt(P.x() * P.x() + P.y() * P.y() + P.z() * P.z());
	}

	void normalize(PointVect &P)
	{
		double nn = norm(P);

		P.setX(P.x() / nn);
		P.setY(P.y() / nn);
		P.setZ(P.z() / nn);
	}

	PointVect cross(PointVect X, PointVect Y)
	{
		return PointVect(X.y() * Y.z() - X.z() * Y.y(),
						 X.z() * Y.x() - X.x() * Y.z(),
						 X.x() * Y.y() - X.y() * Y.x());
	}
};

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	//old stuff
	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void fastNegative();
	void blackWhite();
	void sepiaTone();
	void medianFilter();
	void saltPepper();
	void RotateLeft();
	void RotateRight();
	void Circle(int N, double r,double sx, double sy, int R, int G, int B,int width,int algorithm);
	void DDALine(QPoint P0, QPoint P1, int R, int G, int B,int width);
	void BresenhamLine(QPoint P0, QPoint P1, int R, int G, int B, int width);
	bool dashed = false;
	void PolygonScanline(QPoint *coords,int N, QColor fill);
	void PolygonTransformMove(int N);
	void PolygonTransformScale(int N,int value);
	void PolygonTransformRotate(int N,int value);
	bool vertex_draw = false;
	bool move = false;
	bool scale = false;
	bool rotate = false;
	bool scalexy = false;
	bool shear = false;
	bool reflect = false;
	int N, fpointX, fpointY;
	int sliderVal = 0, sliderLastVal = 0;
	int sliderVal2 = 0, sliderLastVal2 = 0;
	int sliderVal3 = 0, sliderLastVal3 = 0;
	int sliderVal4 = 0, sliderLastVal4 = 0;
	int sliderVal_Combined = 0, sliderLastVal_Combined = 0;

	QPoint StartingPt, EndingPt;
	std::vector<QPoint> vertices;
	//-----------------------------------------------------------------------------------------------
	//---------new stuff ----------------------------------------------------------------------------

	// ----- vector stuff -----
	double dot(PointVect P0, PointVect P1)
	{
		return (P0.x() * P1.x() + P0.y() * P1.y() + P0.z() * P1.z());
	}

	double norm(PointVect P)
	{
		return sqrt(P.x() * P.x() + P.y() * P.y() + P.z() * P.z());
	}

	void normalize(PointVect &P)
	{
		double nn = norm(P);

		P.setX(P.x() / nn);
		P.setY(P.y() / nn);
		P.setZ(P.z() / nn);
	}
	PointVect cross(PointVect X, PointVect Y)
	{
		return PointVect(X.y() * Y.z() - X.z() * Y.y(),
			X.z() * Y.x() - X.x() * Y.z(),
			X.x() * Y.y() - X.y() * Y.x());
	}

	//scene initialization
	void sceneInit();

	//camera
	Camera ourCamera;

	//mesh
	int divisions;

	std::vector<PointVect> MeshPts;
	std::vector<PointVect> ControlPts;
	std::vector<Triangle> Surfaces;
	std::vector<PointVect> ThreePt;
	QColor surfaceBaseColor = QColor::fromRgb(10, 10, 100);

	//object info
	bool objectSphere = false;
	bool objectBezier = false;

	double **DepthBuffer;

	//display info
	bool perspective = false;
	bool parallel = false;
	bool Gouraud = false;	
	bool viewMode_PointCloud = false;
	bool viewMode_Wireframe = true;
	bool viewMode_Surfaces = false;
	bool shading = false; //turns on when flat and Gouraud shading are picked
	bool gouraud = false; //turns on when Gouraud shading is picked
	bool cameraRotation = true; //switches between rotating camera around sphere and rotating the sphere (keeping the lights stationary)
	//-------------------
	bool displayBezierControlPoints = true;
	void MapControlPoints();
	void writeIndex(int i, QPoint position);

	//lights
	std::vector<Light> Lights;
	std::vector<Light> MovingLights;

	PointVect ReflectionVector(Light L, PointVect from, PointVect normal);

	//observer (camera) vector
	PointVect observerVector;

	//default shader parameters
	QColor Diffusion = QColor::fromRgb(255, 0, 0);
	QColor Specular = QColor::fromRgb(200, 200, 200);
	QColor Ambient = QColor::fromRgb(15, 15, 15);
	double shininess = 1.;

	int surfaceIndex;
	PointVect surfaceNormal;
	PointVect surfaceCenter;
	QColor sColor;

	//camera settings
	double CameraDistance = 11.5;
	double FocalDistance = 1.1;
	double PlaneDistance;

	// ---------------------------------------------------------
	//------------------- important functions ------------------

	//surface generation
	void GenerateSphere(int N, double A, double B, double C);
	void GenerateBezierSurface(int N);

	bool defaultBezier = true;

	int NCtrlPtsX = 4;
	int NCtrlPtsY = 3; //number of rows/columns of control points for a Bezier surface

	double defaultGap = 0.5; //default gap size on the xy plane between control points

	//surface mapping (onto the screen/canvas)
	void MapMesh();

	//-------transformations---------------
	void MeshTransformRotate(int value1, int value2);


	//void MapSphere(bool perspective, bool parallel);
	//void SphereTransformRotate(int value1, int value2);
	//void SphereRotateAzimuth(int value); //phi
	//void SphereRotateZenith(int value); //theta

	//-----mesh face filling methods-------------------------------------

	//no shading version, just canvas coords and distances
	void PixelTriangleScanline(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2);
	//flat shading version, reference to a surface normal, canvas coords and distances
	void PixelTriangleScanline(PointVect *surfNormal, PointVect *center, QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2);
	//Gouraud shading version, references to vertices and their normals, canvas coords and distances
	void PixelTriangleScanline(PointVect *p0, PointVect *p1, PointVect *p2, 
							   PointVect *n0, PointVect *n1, PointVect *n2, 
							   QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2);

	//no shading
	void upperTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2);
	void lowerTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2);
	//flat shading
	void upperTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor surfaceColor);
	void lowerTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor surfaceColor);
	//gouraud shading
	void upperTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor C0, QColor C1, QColor C2);
	void lowerTriangleFill(QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2, QColor C0, QColor C1, QColor C2);

	bool insideCanvas(QPoint X);
	bool insideCanvas(int x, int y);
	bool isZeroArea(QPoint V0, QPoint V1, QPoint V2);
	void bufferBetweenPts(QPoint V0, QPoint V1, double d0, double d1);
	double interpolateDistance(int x, int y, QPoint V0, QPoint V1, QPoint V2, double d0, double d1, double d2);
	QColor interpolateColor(int x, int y, QPoint V0, QPoint V1, QPoint V2, QColor C0, QColor C1, QColor C2);

	//------------color stuff -----------------------------------
	int RGBwithinRange(double value);
	QColor computeColor(PointVect *ptOfIncidence, PointVect *normal);

	//-----------------------------------------------------------------------------------------------

	//some other functions
	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);
	int mvX, mvY;
	int xmax, ymax;
	int xmin, ymin;
	double scaledX, scaledY;
	double rotatedX, rotatedY, rotatedZ;
	double radian;
	double thetaGlobal, phiGlobal;
	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QImage image;
	QPoint lastPoint;
};

#endif // PAINTWIDGET_H
