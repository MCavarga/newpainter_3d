#pragma once
class Edgy
{
public:
	Edgy();
	~Edgy();
	Edgy(int x0, int y0, int x1, int y1);
	void SetW (double w);
	int X0() const;
	int Y0() const;
	int X1() const;
	int Y1() const;
	int XL() const;
	double W() const;
	void swap_pts();
private:
	int x0;
	int y0;
	int x1;
	int y1;
	double w;
};

