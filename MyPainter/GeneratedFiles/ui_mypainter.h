/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_4;
    QFormLayout *formLayout_4;
    QLabel *label_15;
    QLabel *label_16;
    QGroupBox *groupBox_5;
    QDoubleSpinBox *doubleSpinBox;
    QDoubleSpinBox *doubleSpinBox_2;
    QDoubleSpinBox *doubleSpinBox_3;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_18;
    QSpinBox *spinBox;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QGroupBox *groupBox;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_1;
    QFrame *frame;
    QSlider *horizontalSlider_3;
    QSlider *horizontalSlider_4;
    QLabel *label_9;
    QLabel *label_2;
    QGroupBox *groupBox_12;
    QSpinBox *spinBox_13;
    QSpinBox *spinBox_14;
    QSpinBox *spinBox_15;
    QGroupBox *groupBox_3;
    QRadioButton *radioButton;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QFrame *line;
    QCheckBox *checkBox_3;
    QGroupBox *groupBox_2;
    QSlider *horizontalSlider;
    QSlider *horizontalSlider_2;
    QLabel *label_10;
    QLabel *label_11;
    QCheckBox *checkBox;
    QGroupBox *groupBox_6;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_5;
    QGroupBox *groupBox_7;
    QSpinBox *spinBox_4;
    QSpinBox *spinBox_5;
    QSpinBox *spinBox_6;
    QGroupBox *groupBox_8;
    QSpinBox *spinBox_7;
    QSpinBox *spinBox_8;
    QSpinBox *spinBox_9;
    QGroupBox *groupBox_9;
    QSpinBox *spinBox_10;
    QSpinBox *spinBox_11;
    QSpinBox *spinBox_12;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QGroupBox *groupBox_10;
    QDoubleSpinBox *doubleSpinBox_4;
    QTableWidget *tableWidget_3;
    QLabel *label_14;
    QPushButton *pushButton_5;
    QGroupBox *groupBox_11;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_6;
    QGroupBox *groupBox_4;
    QScrollArea *scrollArea_3;
    QWidget *scrollAreaWidgetContents_6;
    QPushButton *pushButton_3;
    QPushButton *pushButton_9;
    QLabel *label;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QCheckBox *checkBox_2;
    QPushButton *pushButton_10;
    QLabel *label_3;
    QSpinBox *spinBox_2;
    QSpinBox *spinBox_3;
    QTableWidget *tableWidget;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEffects;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(1129, 847);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        formLayout_4 = new QFormLayout();
        formLayout_4->setSpacing(6);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        formLayout_4->setContentsMargins(4, 4, 4, 4);
        label_15 = new QLabel(centralWidget);
        label_15->setObjectName(QStringLiteral("label_15"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_15);

        label_16 = new QLabel(centralWidget);
        label_16->setObjectName(QStringLiteral("label_16"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, label_16);

        groupBox_5 = new QGroupBox(centralWidget);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setMinimumSize(QSize(200, 55));
        doubleSpinBox = new QDoubleSpinBox(groupBox_5);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(10, 20, 51, 22));
        doubleSpinBox->setMinimum(0.1);
        doubleSpinBox->setMaximum(3);
        doubleSpinBox->setSingleStep(0.01);
        doubleSpinBox->setValue(1);
        doubleSpinBox_2 = new QDoubleSpinBox(groupBox_5);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setGeometry(QRect(70, 20, 51, 22));
        doubleSpinBox_2->setMinimum(0.1);
        doubleSpinBox_2->setMaximum(3);
        doubleSpinBox_2->setSingleStep(0.01);
        doubleSpinBox_2->setValue(0.85);
        doubleSpinBox_3 = new QDoubleSpinBox(groupBox_5);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setGeometry(QRect(130, 20, 51, 22));
        doubleSpinBox_3->setMinimum(0.1);
        doubleSpinBox_3->setMaximum(3);
        doubleSpinBox_3->setSingleStep(0.01);
        doubleSpinBox_3->setValue(0.5);

        formLayout_4->setWidget(1, QFormLayout::LabelRole, groupBox_5);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_18 = new QLabel(centralWidget);
        label_18->setObjectName(QStringLiteral("label_18"));

        verticalLayout_4->addWidget(label_18);

        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMinimum(1);
        spinBox->setMaximum(50);
        spinBox->setValue(8);

        verticalLayout_4->addWidget(spinBox);


        formLayout_4->setLayout(1, QFormLayout::FieldRole, verticalLayout_4);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setMinimumSize(QSize(200, 0));

        formLayout_4->setWidget(2, QFormLayout::LabelRole, pushButton);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        formLayout_4->setWidget(2, QFormLayout::FieldRole, pushButton_2);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(200, 200));
        radioButton_2 = new QRadioButton(groupBox);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setGeometry(QRect(10, 20, 69, 20));
        radioButton_2->setCheckable(true);
        radioButton_1 = new QRadioButton(groupBox);
        radioButton_1->setObjectName(QStringLiteral("radioButton_1"));
        radioButton_1->setGeometry(QRect(10, 50, 92, 20));
        radioButton_1->setCheckable(true);
        radioButton_1->setChecked(false);
        frame = new QFrame(groupBox);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setEnabled(true);
        frame->setGeometry(QRect(10, 80, 181, 91));
        frame->setMinimumSize(QSize(150, 90));
        frame->setBaseSize(QSize(80, 60));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalSlider_3 = new QSlider(frame);
        horizontalSlider_3->setObjectName(QStringLiteral("horizontalSlider_3"));
        horizontalSlider_3->setGeometry(QRect(10, 60, 161, 22));
        horizontalSlider_3->setMinimum(25);
        horizontalSlider_3->setMaximum(150);
        horizontalSlider_3->setSingleStep(1);
        horizontalSlider_3->setPageStep(1);
        horizontalSlider_3->setValue(100);
        horizontalSlider_3->setOrientation(Qt::Horizontal);
        horizontalSlider_4 = new QSlider(frame);
        horizontalSlider_4->setObjectName(QStringLiteral("horizontalSlider_4"));
        horizontalSlider_4->setEnabled(true);
        horizontalSlider_4->setGeometry(QRect(10, 20, 161, 21));
        horizontalSlider_4->setMinimum(0);
        horizontalSlider_4->setMaximum(50);
        horizontalSlider_4->setOrientation(Qt::Horizontal);
        label_9 = new QLabel(frame);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 0, 171, 16));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 40, 131, 16));
        horizontalSlider_3->raise();
        label_9->raise();
        label_2->raise();
        horizontalSlider_4->raise();
        groupBox_12 = new QGroupBox(groupBox);
        groupBox_12->setObjectName(QStringLiteral("groupBox_12"));
        groupBox_12->setGeometry(QRect(0, 170, 171, 51));
        spinBox_13 = new QSpinBox(groupBox_12);
        spinBox_13->setObjectName(QStringLiteral("spinBox_13"));
        spinBox_13->setGeometry(QRect(10, 20, 42, 22));
        spinBox_13->setMaximum(255);
        spinBox_14 = new QSpinBox(groupBox_12);
        spinBox_14->setObjectName(QStringLiteral("spinBox_14"));
        spinBox_14->setGeometry(QRect(60, 20, 42, 22));
        spinBox_14->setMaximum(255);
        spinBox_14->setValue(10);
        spinBox_15 = new QSpinBox(groupBox_12);
        spinBox_15->setObjectName(QStringLiteral("spinBox_15"));
        spinBox_15->setGeometry(QRect(110, 20, 42, 22));
        spinBox_15->setMaximum(255);
        spinBox_15->setValue(70);

        formLayout_4->setWidget(3, QFormLayout::LabelRole, groupBox);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(220, 160));
        radioButton = new QRadioButton(groupBox_3);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(20, 20, 111, 17));
        radioButton_3 = new QRadioButton(groupBox_3);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setGeometry(QRect(20, 40, 101, 17));
        radioButton_3->setChecked(true);
        radioButton_4 = new QRadioButton(groupBox_3);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setGeometry(QRect(20, 60, 161, 17));
        radioButton_5 = new QRadioButton(groupBox_3);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));
        radioButton_5->setGeometry(QRect(20, 80, 171, 20));
        radioButton_6 = new QRadioButton(groupBox_3);
        radioButton_6->setObjectName(QStringLiteral("radioButton_6"));
        radioButton_6->setGeometry(QRect(20, 100, 191, 20));
        line = new QFrame(groupBox_3);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(0, 120, 211, 16));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        checkBox_3 = new QCheckBox(groupBox_3);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setEnabled(true);
        checkBox_3->setGeometry(QRect(10, 130, 191, 20));
        checkBox_3->setChecked(true);

        formLayout_4->setWidget(4, QFormLayout::LabelRole, groupBox_3);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setEnabled(false);
        groupBox_2->setMinimumSize(QSize(150, 125));
        horizontalSlider = new QSlider(groupBox_2);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(30, 40, 251, 22));
        horizontalSlider->setMaximum(360);
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider_2 = new QSlider(groupBox_2);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        horizontalSlider_2->setGeometry(QRect(30, 90, 241, 22));
        horizontalSlider_2->setMinimum(-90);
        horizontalSlider_2->setMaximum(90);
        horizontalSlider_2->setOrientation(Qt::Horizontal);
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(10, 20, 111, 16));
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(10, 70, 81, 16));
        checkBox = new QCheckBox(groupBox_2);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(10, 120, 151, 20));
        checkBox->setChecked(true);

        formLayout_4->setWidget(4, QFormLayout::FieldRole, groupBox_2);

        groupBox_6 = new QGroupBox(centralWidget);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setMinimumSize(QSize(0, 250));
        scrollArea_2 = new QScrollArea(groupBox_6);
        scrollArea_2->setObjectName(QStringLiteral("scrollArea_2"));
        scrollArea_2->setGeometry(QRect(0, 16, 521, 231));
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_5 = new QWidget();
        scrollAreaWidgetContents_5->setObjectName(QStringLiteral("scrollAreaWidgetContents_5"));
        scrollAreaWidgetContents_5->setGeometry(QRect(0, 0, 519, 229));
        groupBox_7 = new QGroupBox(scrollAreaWidgetContents_5);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setGeometry(QRect(0, 70, 191, 51));
        spinBox_4 = new QSpinBox(groupBox_7);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        spinBox_4->setGeometry(QRect(10, 20, 51, 22));
        spinBox_4->setMaximum(255);
        spinBox_4->setValue(200);
        spinBox_5 = new QSpinBox(groupBox_7);
        spinBox_5->setObjectName(QStringLiteral("spinBox_5"));
        spinBox_5->setGeometry(QRect(70, 20, 51, 22));
        spinBox_5->setMaximum(255);
        spinBox_5->setValue(100);
        spinBox_6 = new QSpinBox(groupBox_7);
        spinBox_6->setObjectName(QStringLiteral("spinBox_6"));
        spinBox_6->setGeometry(QRect(130, 20, 51, 22));
        spinBox_6->setMaximum(255);
        spinBox_6->setValue(40);
        groupBox_8 = new QGroupBox(scrollAreaWidgetContents_5);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        groupBox_8->setGeometry(QRect(0, 120, 191, 51));
        spinBox_7 = new QSpinBox(groupBox_8);
        spinBox_7->setObjectName(QStringLiteral("spinBox_7"));
        spinBox_7->setGeometry(QRect(10, 20, 51, 22));
        spinBox_7->setMaximum(255);
        spinBox_7->setSingleStep(1);
        spinBox_7->setValue(255);
        spinBox_8 = new QSpinBox(groupBox_8);
        spinBox_8->setObjectName(QStringLiteral("spinBox_8"));
        spinBox_8->setGeometry(QRect(70, 20, 51, 22));
        spinBox_8->setMaximum(255);
        spinBox_8->setValue(255);
        spinBox_9 = new QSpinBox(groupBox_8);
        spinBox_9->setObjectName(QStringLiteral("spinBox_9"));
        spinBox_9->setGeometry(QRect(130, 20, 51, 22));
        spinBox_9->setMaximum(255);
        spinBox_9->setValue(255);
        groupBox_9 = new QGroupBox(scrollAreaWidgetContents_5);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        groupBox_9->setGeometry(QRect(0, 170, 191, 51));
        spinBox_10 = new QSpinBox(groupBox_9);
        spinBox_10->setObjectName(QStringLiteral("spinBox_10"));
        spinBox_10->setGeometry(QRect(10, 20, 51, 22));
        spinBox_10->setMaximum(255);
        spinBox_11 = new QSpinBox(groupBox_9);
        spinBox_11->setObjectName(QStringLiteral("spinBox_11"));
        spinBox_11->setGeometry(QRect(70, 20, 51, 22));
        spinBox_11->setMaximum(255);
        spinBox_12 = new QSpinBox(groupBox_9);
        spinBox_12->setObjectName(QStringLiteral("spinBox_12"));
        spinBox_12->setGeometry(QRect(130, 20, 51, 22));
        spinBox_12->setMaximum(255);
        pushButton_6 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(100, 40, 93, 28));
        pushButton_7 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(100, 10, 93, 28));
        pushButton_8 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setGeometry(QRect(0, 40, 93, 28));
        groupBox_10 = new QGroupBox(scrollAreaWidgetContents_5);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        groupBox_10->setGeometry(QRect(200, 170, 81, 51));
        doubleSpinBox_4 = new QDoubleSpinBox(groupBox_10);
        doubleSpinBox_4->setObjectName(QStringLiteral("doubleSpinBox_4"));
        doubleSpinBox_4->setGeometry(QRect(10, 20, 61, 22));
        doubleSpinBox_4->setSingleStep(0.25);
        doubleSpinBox_4->setValue(1);
        tableWidget_3 = new QTableWidget(scrollAreaWidgetContents_5);
        tableWidget_3->setObjectName(QStringLiteral("tableWidget_3"));
        tableWidget_3->setGeometry(QRect(200, 30, 311, 131));
        label_14 = new QLabel(scrollAreaWidgetContents_5);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(200, 10, 89, 16));
        pushButton_5 = new QPushButton(scrollAreaWidgetContents_5);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(0, 10, 93, 28));
        groupBox_11 = new QGroupBox(scrollAreaWidgetContents_5);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        groupBox_11->setGeometry(QRect(290, 169, 221, 51));
        lineEdit_4 = new QLineEdit(groupBox_11);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(0, 20, 71, 22));
        lineEdit_5 = new QLineEdit(groupBox_11);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(70, 20, 71, 22));
        lineEdit_6 = new QLineEdit(groupBox_11);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(140, 20, 71, 22));
        scrollArea_2->setWidget(scrollAreaWidgetContents_5);

        formLayout_4->setWidget(5, QFormLayout::SpanningRole, groupBox_6);

        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setMinimumSize(QSize(300, 220));
        scrollArea_3 = new QScrollArea(groupBox_4);
        scrollArea_3->setObjectName(QStringLiteral("scrollArea_3"));
        scrollArea_3->setGeometry(QRect(0, 20, 291, 200));
        scrollArea_3->setMinimumSize(QSize(195, 200));
        scrollArea_3->setWidgetResizable(true);
        scrollAreaWidgetContents_6 = new QWidget();
        scrollAreaWidgetContents_6->setObjectName(QStringLiteral("scrollAreaWidgetContents_6"));
        scrollAreaWidgetContents_6->setGeometry(QRect(0, 0, 289, 198));
        pushButton_3 = new QPushButton(scrollAreaWidgetContents_6);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(180, 40, 93, 28));
        pushButton_9 = new QPushButton(scrollAreaWidgetContents_6);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));
        pushButton_9->setGeometry(QRect(180, 70, 93, 28));
        label = new QLabel(scrollAreaWidgetContents_6);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 50, 171, 16));
        lineEdit = new QLineEdit(scrollAreaWidgetContents_6);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(0, 70, 51, 22));
        lineEdit_2 = new QLineEdit(scrollAreaWidgetContents_6);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(60, 70, 51, 22));
        lineEdit_3 = new QLineEdit(scrollAreaWidgetContents_6);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(120, 70, 51, 22));
        checkBox_2 = new QCheckBox(scrollAreaWidgetContents_6);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(100, 20, 71, 31));
        checkBox_2->setChecked(true);
        pushButton_10 = new QPushButton(scrollAreaWidgetContents_6);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));
        pushButton_10->setGeometry(QRect(180, 10, 93, 28));
        label_3 = new QLabel(scrollAreaWidgetContents_6);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(1, 1, 175, 16));
        spinBox_2 = new QSpinBox(scrollAreaWidgetContents_6);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setGeometry(QRect(0, 20, 41, 22));
        spinBox_2->setMinimum(2);
        spinBox_2->setMaximum(20);
        spinBox_2->setValue(4);
        spinBox_3 = new QSpinBox(scrollAreaWidgetContents_6);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setGeometry(QRect(50, 20, 41, 22));
        spinBox_3->setMinimum(1);
        spinBox_3->setMaximum(20);
        spinBox_3->setValue(3);
        tableWidget = new QTableWidget(scrollAreaWidgetContents_6);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(10, 100, 271, 91));
        scrollArea_3->setWidget(scrollAreaWidgetContents_6);

        formLayout_4->setWidget(3, QFormLayout::FieldRole, groupBox_4);


        horizontalLayout_4->addLayout(formLayout_4);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 561, 770));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        horizontalLayout_4->addWidget(scrollArea);


        gridLayout->addLayout(horizontalLayout_4, 0, 0, 1, 1);

        MyPainterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1129, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuEffects = new QMenu(menuBar);
        menuEffects->setObjectName(QStringLiteral("menuEffects"));
        MyPainterClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEffects->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuEffects->addAction(actionClear);
        menuEffects->addAction(actionNegative);
        menuEffects->addAction(actionMedium_Negative);
        menuEffects->addAction(actionSlow_Negative);
        menuEffects->addAction(actionSalt_Pepper);
        menuEffects->addAction(actionMedian_filter);
        menuEffects->addAction(actionRotate_left);
        menuEffects->addAction(actionRotate_right);

        retranslateUi(MyPainterClass);
        QObject::connect(actionOpen, SIGNAL(triggered()), MyPainterClass, SLOT(ActionOpen()));
        QObject::connect(actionSave, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSave()));
        QObject::connect(actionNew, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNew()));
        QObject::connect(actionNegative, SIGNAL(triggered()), MyPainterClass, SLOT(ActionNegative()));
        QObject::connect(actionSlow_Negative, SIGNAL(triggered()), MyPainterClass, SLOT(ActionBlackWhite()));
        QObject::connect(actionMedium_Negative, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSepiaTone()));
        QObject::connect(actionMedian_filter, SIGNAL(triggered()), MyPainterClass, SLOT(ActionMedian()));
        QObject::connect(actionSalt_Pepper, SIGNAL(triggered()), MyPainterClass, SLOT(ActionSaltPepper()));
        QObject::connect(actionRotate_left, SIGNAL(triggered()), MyPainterClass, SLOT(ActionLeft()));
        QObject::connect(actionRotate_right, SIGNAL(triggered()), MyPainterClass, SLOT(ActionRight()));
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(ActionGenerateSphere()));
        QObject::connect(radioButton_1, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionProjectPerspective()));
        QObject::connect(radioButton_2, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionProjectParallel()));
        QObject::connect(horizontalSlider, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(ActionValueChanged()));
        QObject::connect(horizontalSlider_2, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(ActionValueChanged()));
        QObject::connect(horizontalSlider_3, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(ActionValueChanged()));
        QObject::connect(horizontalSlider_4, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(ActionValueChanged()));
        QObject::connect(radioButton_4, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionViewMode_Surfaces()));
        QObject::connect(radioButton_3, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionViewMode_Wireframe()));
        QObject::connect(radioButton, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionViewMode_PointCloud()));
        QObject::connect(radioButton_5, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionShadingFlat()));
        QObject::connect(radioButton_6, SIGNAL(clicked(bool)), MyPainterClass, SLOT(ActionShadingGouraud()));
        QObject::connect(checkBox, SIGNAL(stateChanged(int)), MyPainterClass, SLOT(ActionEnableCameraRotation()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), MyPainterClass, SLOT(ActionAddLight()));
        QObject::connect(pushButton_7, SIGNAL(clicked()), MyPainterClass, SLOT(ActionRemoveLastLight()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), MyPainterClass, SLOT(ActionClearLights()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(ActionGenerateBezier()));
        QObject::connect(pushButton_10, SIGNAL(clicked()), MyPainterClass, SLOT(ActionLoadBezier()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(ActionAddControlPoint()));
        QObject::connect(pushButton_9, SIGNAL(clicked()), MyPainterClass, SLOT(ActionRemoveLastControlPoint()));
        QObject::connect(checkBox_3, SIGNAL(stateChanged(int)), MyPainterClass, SLOT(ActionDisplayBezierControlPts()));
        QObject::connect(pushButton_8, SIGNAL(clicked()), MyPainterClass, SLOT(ActionUpdateLightColors()));
        QObject::connect(checkBox_2, SIGNAL(stateChanged(int)), MyPainterClass, SLOT(ActionSwitchDefaultBezier()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", Q_NULLPTR));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", Q_NULLPTR));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", Q_NULLPTR));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", Q_NULLPTR));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", Q_NULLPTR));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", Q_NULLPTR));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", Q_NULLPTR));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", Q_NULLPTR));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", Q_NULLPTR));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", Q_NULLPTR));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", Q_NULLPTR));
        label_15->setText(QApplication::translate("MyPainterClass", "Ellipsoid stuff: ", Q_NULLPTR));
        label_16->setText(QString());
        groupBox_5->setTitle(QApplication::translate("MyPainterClass", "Semi-axes:", Q_NULLPTR));
        label_18->setText(QApplication::translate("MyPainterClass", "N of surface segments:", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MyPainterClass", "Generate Ellipsoid", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MyPainterClass", "Generate Bezier Surface", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MyPainterClass", "Projection:", Q_NULLPTR));
        radioButton_2->setText(QApplication::translate("MyPainterClass", "Parallel", Q_NULLPTR));
        radioButton_1->setText(QApplication::translate("MyPainterClass", "Perspective", Q_NULLPTR));
        label_9->setText(QApplication::translate("MyPainterClass", "Field of view (focal distance):", Q_NULLPTR));
        label_2->setText(QApplication::translate("MyPainterClass", "Camera distance:", Q_NULLPTR));
        groupBox_12->setTitle(QApplication::translate("MyPainterClass", "Surface base color:", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("MyPainterClass", "View mode:", Q_NULLPTR));
        radioButton->setText(QApplication::translate("MyPainterClass", "Point Cloud", Q_NULLPTR));
        radioButton_3->setText(QApplication::translate("MyPainterClass", "Wireframe", Q_NULLPTR));
        radioButton_4->setText(QApplication::translate("MyPainterClass", "Surfaces (No Shading)", Q_NULLPTR));
        radioButton_5->setText(QApplication::translate("MyPainterClass", "Surfaces (Flat Shading)", Q_NULLPTR));
        radioButton_6->setText(QApplication::translate("MyPainterClass", "Surfaces (Gouraud Shading)", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("MyPainterClass", "(Bezier) display control pts", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MyPainterClass", "Rotations Ctrl:", Q_NULLPTR));
        label_10->setText(QApplication::translate("MyPainterClass", "0 < phi < 2*Pi:", Q_NULLPTR));
        label_11->setText(QApplication::translate("MyPainterClass", "0 < theta < Pi:", Q_NULLPTR));
        checkBox->setText(QApplication::translate("MyPainterClass", "Rotate camera", Q_NULLPTR));
        groupBox_6->setTitle(QApplication::translate("MyPainterClass", "Light Settings:", Q_NULLPTR));
        groupBox_7->setTitle(QApplication::translate("MyPainterClass", "Diffuse light (R,G,B):", Q_NULLPTR));
        groupBox_8->setTitle(QApplication::translate("MyPainterClass", "Specular light (R,G,B):", Q_NULLPTR));
        groupBox_9->setTitle(QApplication::translate("MyPainterClass", "Ambient light (R,G,B):", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("MyPainterClass", "Clear Lights", Q_NULLPTR));
        pushButton_7->setText(QApplication::translate("MyPainterClass", "Remove last", Q_NULLPTR));
        pushButton_8->setText(QApplication::translate("MyPainterClass", "Update colors", Q_NULLPTR));
        groupBox_10->setTitle(QApplication::translate("MyPainterClass", "Shininess:", Q_NULLPTR));
        label_14->setText(QApplication::translate("MyPainterClass", "Table of Lights:", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("MyPainterClass", "Add Light", Q_NULLPTR));
        groupBox_11->setTitle(QApplication::translate("MyPainterClass", "Position (x, y, z):", Q_NULLPTR));
        lineEdit_4->setText(QApplication::translate("MyPainterClass", "0", Q_NULLPTR));
        lineEdit_5->setText(QApplication::translate("MyPainterClass", "0", Q_NULLPTR));
        lineEdit_6->setText(QApplication::translate("MyPainterClass", "2", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("MyPainterClass", "Bezier Stuff:", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("MyPainterClass", "Add point", Q_NULLPTR));
        pushButton_9->setText(QApplication::translate("MyPainterClass", "Remove last", Q_NULLPTR));
        label->setText(QApplication::translate("MyPainterClass", "ctrl. pt. coords (x, y, z):", Q_NULLPTR));
        checkBox_2->setText(QApplication::translate("MyPainterClass", "Default", Q_NULLPTR));
        pushButton_10->setText(QApplication::translate("MyPainterClass", "Load file", Q_NULLPTR));
        label_3->setText(QApplication::translate("MyPainterClass", "m x n (control points):", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("MyPainterClass", "File", Q_NULLPTR));
        menuEffects->setTitle(QApplication::translate("MyPainterClass", "Effects", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
